# USP Daemon (uspd)

`uspd` is data-model daemon which exposes data-model objects over ubus as required
by [TR-069/cwmp](https://cwmp-data-models.broadband-forum.org/) or [TR-369/USP](https://usp.technology/),
`uspd` also understand [USP syntax](https://usp.technology/specification/architecture/) as defined in R-ARC.7 - R-ARC.12
and provide details for data-model queries.

> Note 1: The command outputs shown in this readme are examples only, actual output may differ based on device and configuration.

> Note 2: Long command outputs are compressed for better readability

## Project Components

Project consists of following components:

- Application itself written in C programming language
- Documentation in a Markdown format

## Build Instructions

`uspd` is written using C programming language and depends on a number of components found in `OpenWrt` for building and running.

## UCI Config

`uspd` requires a configuration file to provide more granular objects over `ubus`.
Granularity is an optional feature of `uspd`, it can be skipped or set to level 0.
The configuration file is an `uci` file `/etc/config/uspd`. Sample configuration file is provided below.

```bash
config uspd 'usp'
        option granularitylevel  '0'
        option debug 'true'
        option loglevel  '2'
        option sock '/tmp/usp.sock'
        option transaction_timeout 60
```

In the above uci, loglevel can have below value:

|loglevel |  Meaning                                 |
|---------| -----------------------------------------|
|  0      | Disabled logging                         |
|  1      | Only errors will be logged               |
|  2      | Only errors and warnings will be logged  |
|  3      | Log everything except debug              |
|  4      | Everything will be logged                |

For more info on the `uspd` UCI configuration see [link](./docs/api/uci.uspd.md)

## Ubus API

`uspd` needs to be started on startup after `ubusd`, as it exposes the data-model
objects over `ubus`. By default(when granularity is not set in `uci`), `uspd` registers
below two namespaces with `ubus`.

```console
root@iopsys:~# ubus list |grep usp
usp
usp.raw
```

`usp` namespace is to provide the output as required by `End User` or in pretty format,
whereas `usp.raw` namespace is to provide output in raw JSON format for easy API integration,
which can be used by other USP front-end applications(like: `obuspa`, `icwmp`).
`usp` namespace has fewer methods defined to provide a simple interface to `end users`,
whereas `usp.raw` has more features/methods to provide more customization options.

Default namespace with functionalities:

```console
root@iopsys:~# ubus -v list usp
'usp' @78f3eaca
        "list_operate":{}
        "get":{"path":"String","proto":"String","maxdepth":"Integer","next-level":"Boolean","instance_mode":"Integer"}
        "object_names":{"path":"String","proto":"String","maxdepth":"Integer","next-level":"Boolean","instance_mode":"Integer"}
        "instances":{"path":"String","proto":"String","maxdepth":"Integer","next-level":"Boolean","instance_mode":"Integer"}
        "validate":{"path":"String","proto":"String","maxdepth":"Integer","next-level":"Boolean","instance_mode":"Integer"}
        "set":{"path":"String","value":"String","values":"Table","proto":"String","key":"String","instance_mode":"Integer"}
        "operate":{"path":"String","action":"String","input":"Table","proto":"String","instance_mode":"Integer"}
        "add_object":{"path":"String","proto":"String","key":"String","instance_mode":"Integer"}
        "del_object":{"path":"String","proto":"String","key":"String","instance_mode":"Integer"}
root@iopsys:~#
root@iopsys:~# ubus -v list usp.raw
'usp.raw' @08a13407
	"dump_schema":{}
	"list_operate":{}
	"list_events":{}
	"get":{"path":"String","proto":"String","maxdepth":"Integer","next-level":"Boolean","instance_mode":"Integer"}
	"getm_values":{"paths":"Array","proto":"String","next-level":"Boolean","instance_mode":"Integer"}
	"getm_names":{"paths":"Array","proto":"String","next-level":"Boolean","instance_mode":"Integer"}
	"object_names":{"path":"String","proto":"String","maxdepth":"Integer","next-level":"Boolean","instance_mode":"Integer"}
	"instances":{"path":"String","proto":"String","maxdepth":"Integer","next-level":"Boolean","instance_mode":"Integer"}
	"validate":{"path":"String","proto":"String","maxdepth":"Integer","next-level":"Boolean","instance_mode":"Integer"}
	"transaction_start":{"app":"String"}
	"set":{"path":"String","value":"String","values":"Table","proto":"String","key":"String","instance_mode":"Integer","transaction_id":"Integer"}
	"operate":{"path":"String","action":"String","input":"Table","proto":"String","instance_mode":"Integer"}
	"add_object":{"path":"String","proto":"String","key":"String","instance_mode":"Integer","transaction_id":"Integer"}
	"del_object":{"path":"String","proto":"String","key":"String","instance_mode":"Integer","transaction_id":"Integer"}
	"setm_values":{"pv_tuple":"Array","proto":"String","instance_mode":"Integer","key":"String","transaction_id":"Integer"}
	"transaction_commit":{"transaction_id":"Integer"}
	"transaction_abort":{"transaction_id":"Integer"}
	"transaction_status":{"transaction_id":"Integer"}
	"notify_event":{"name":"String","input":"Table"}
root@iopsys:~#
```

> Note1: `proto` in each method specify the data-model prototype('cwmp', 'usp') to use, if not provided default data-model will be used.

> Note2: `instance_mode` could be 0 or 1, for instance number, instance alias respectively.

> Note3: `next-level` true means only get next level objects and false means get all objects recursively

> Note4: `maxdepth` is measured on max number of .(Dot) present in object name

> Note5: `key` is used specifically for cwmp param_key

The objects registered with the above namespaces can be called with appropriate
parameters to perform a USP `Get/Set/Operate/Add Object/Delete Object` operation as below.

## Granularity

Granularity feature is basically exposes the same USP functionality by registering
additional ubus namespaces to reduce the `path` length in ubus parameter.
It is the number of levels(Dots) up to which we want to shorten the length.

Ex:
 - When Granularity is set to 1, exposed ubus namespaces are

```console
root@iopsys:~# ubus list|grep usp
usp
usp.Device.
usp.raw
```

 - When Granularity is set to 2, exposed ubus namespaces are

```console
root@iopsys:~# ubus list|grep usp
usp
usp.Device.
usp.Device.Bridging.
usp.Device.DHCPv4.
usp.Device.DHCPv6.
usp.Device.DNS.
usp.Device.DeviceInfo.
usp.Device.DynamicDNS.
usp.Device.Ethernet.
usp.Device.Firewall.
usp.Device.Hosts.
usp.Device.IP.
usp.raw
root@iopsys:~#
```

These granular ubus objects provides the same functionality as of `usp` ubus namespace

```console
root@iopsys:~# ubus -v list usp.Device.WiFi.
'usp.Device.WiFi.' @6fd43aca
        "list_operate":{}
        "get":{"path":"String","proto":"String","maxdepth":"Integer","next-level":"Boolean","instance_mode":"Integer"}
        "object_names":{"path":"String","proto":"String","maxdepth":"Integer","next-level":"Boolean","instance_mode":"Integer"}
        "instances":{"path":"String","proto":"String","maxdepth":"Integer","next-level":"Boolean","instance_mode":"Integer"}
        "validate":{"path":"String","proto":"String","maxdepth":"Integer","next-level":"Boolean","instance_mode":"Integer"}
        "set":{"path":"String","value":"String","values":"Table","proto":"String","key":"String","instance_mode":"Integer"}
        "operate":{"path":"String","action":"String","input":"Table","proto":"String","instance_mode":"Integer"}
        "add_object":{"path":"String","proto":"String","key":"String","instance_mode":"Integer"}
        "del_object":{"path":"String","proto":"String","key":"String","instance_mode":"Integer"}
root@iopsys:~#
```

Registered method can be called with appropriate parameters, like:

```console
root@iopsys:~# ubus call usp.Device. get '{"path":"Users."}'
{
        "Users": {
                "User": [
                        {
                                "Alias": "cpe-1",
                                "Enable": true,
                                "Language": "",
                                "Password": "",
                                "RemoteAccessCapable": true,
                                "Username": "user"
                        },
                        {
                                "Alias": "cpe-2",
                                "Enable": true,
                                "Language": "",
                                "Password": "",
                                "RemoteAccessCapable": true,
                                "Username": "support"
                        },
                        {
                                "Alias": "cpe-3",
                                "Enable": true,
                                "Language": "",
                                "Password": "",
                                "RemoteAccessCapable": true,
                                "Username": "admin"
                        }
                ],
                "UserNumberOfEntries": 3
        }
}
root@iopsys:~#
```

## Overview

`uspd` provides below commands in pretty(usp) or raw(usp.raw) formats, some methods only available for API integration in usp.raw namespace:

   - Get
   - Get multiple values
   - Get multiple names
   - Set
   - Operate
   - Add object
   - Delete object
   - Object names
   - Instances
   - Validate
   - List operate
   - Dump schema
   - Set multiple values
   - Transaction start
   - Transaction status
   - Transaction commit
   - Transaction abort
   - List supported usp events
   - Send notification for an event

It also provide a granularity layer which can be configured using uci parameter and provide additional ubus objects.

### Get
API to query the value of a specific object.

```console
root@iopsys:~# ubus call usp get '{"path":"Device.IP.Diagnostics.", "proto":"usp"}'
{
        "Diagnostics": {
                "IPv4DownloadDiagnosticsSupported": true,
                "IPv4PingSupported": true,
                "IPv4ServerSelectionDiagnosticsSupported": true,
                "IPv4TraceRouteSupported": true,
                "IPv4UDPEchoDiagnosticsSupported": true,
                "IPv4UploadDiagnosticsSupported": true,
                "IPv6DownloadDiagnosticsSupported": true,
                "IPv6PingSupported": true,
                "IPv6ServerSelectionDiagnosticsSupported": true,
                "IPv6TraceRouteSupported": true,
                "IPv6UDPEchoDiagnosticsSupported": true,
                "IPv6UploadDiagnosticsSupported": true
        }
}
root@iopsys:~#
root@iopsys:~# ubus call usp get '{"path":"Device.IP.Diagnostics.", "proto":"cwmp"}'
{
        "Diagnostics": {
                "DownloadDiagnostics": {
                        "BOMTime": "0",
                        "DSCP": 0,
                        "DiagnosticsState": "None",
                        "DownloadDiagnosticMaxConnections": 1,
                        "TotalBytesSent": 0,
                        "TotalBytesSentUnderFullLoading": 0
                },
                "IPPing": {
                        "AverageResponseTime": 0,
                        "AverageResponseTimeDetailed": 0,
                        "DSCP": 0,
                        "DataBlockSize": 64,
                        "ProtocolVersion": "Any",
                        "SuccessCount": 0,
                        "Timeout": 1000
                },
                "IPv4DownloadDiagnosticsSupported": true,
                "IPv4PingSupported": true,
                "IPv4ServerSelectionDiagnosticsSupported": true,
                "IPv6UDPEchoDiagnosticsSupported": true,
                "IPv6UploadDiagnosticsSupported": true,
                }
        }
}
root@iopsys:~#
root@iopsys:~# ubus call usp get '{"path":"Device.Users."}'
{
        "Users": {
                "User": [
                        {
                                "Alias": "cpe-1",
                                "Enable": true,
                                "Language": "",
                                "Password": "",
                                "RemoteAccessCapable": true,
                                "Username": "user"
                        },
                        {
                                "Alias": "cpe-2",
                                "Enable": true,
                                "Language": "",
                                "Password": "",
                                "RemoteAccessCapable": true,
                                "Username": "support"
                        },
                        {
                                "Alias": "cpe-3",
                                "Enable": true,
                                "Language": "",
                                "Password": "",
                                "RemoteAccessCapable": true,
                                "Username": "admin"
                        }
                ],
                "UserNumberOfEntries": 3
        }
}
root@iopsys:~#
root@iopsys:~# ubus call usp.raw get '{"path":"Device.Users."}'
{
        "parameters": [
                {
                        "parameter": "Device.Users.User.1.Alias",
                        "value": "cpe-1",
                        "type": "xsd:string"
                },
                {
                        "parameter": "Device.Users.User.1.Enable",
                        "value": "1",
                        "type": "xsd:boolean"
                },
                {
                        "parameter": "Device.Users.User.1.Language",
                        "value": "",
                        "type": "xsd:string"
                },
                {
                        "parameter": "Device.Users.User.1.Password",
                        "value": "",
                        "type": "xsd:string"
                }
        ]
}

```
- For more info on the `usp` ubus API see [link](./docs/api/usp.md#get)
- For more info on the `usp.raw` ubus API see [link](./docs/api/usp.raw.md#get)

### Get multiple values
API to get values of multiple objects at once, object name must be provided in `paths` parameter array as below.

> Note: This method is only available in `usp.raw` namespace.

```console
root@iopsys:~# ubus call usp.raw getm_values '{"paths":["Device.Users.User.1.Username","Device.DeviceInfo.SerialNumber"]}'
{
        "parameters": [
                {
                        "parameter": "Device.Users.User.1.Username",
                        "value": "user",
                        "type": "xsd:string"
                },
                {
                        "parameter": "Device.DeviceInfo.SerialNumber",
                        "value": "E40A24H185027824",
                        "type": "xsd:string"
                }
        ]
}
```

### Get multiple objects
API to get multiple objects from multiple paths at once.

> Note: This method only available in `usp.raw` namespace

```console
root@iopsys:~# ubus call usp.raw getm_names '{"paths":["Device.Users.User.1.","Device.DeviceInfo.SerialNumber"]}'
{
        "parameters": [
                {
                        "parameter": "Device.Users.User.1.",
                        "value": "1",
                        "type": "xsd:object"
                },
                {
                        "parameter": "Device.Users.User.1.Alias",
                        "value": "1",
                        "type": "xsd:string"
                },
                {
                        "parameter": "Device.Users.User.1.Enable",
                        "value": "1",
                        "type": "xsd:boolean"
                },
                {
                        "parameter": "Device.Users.User.1.Language",
                        "value": "1",
                        "type": "xsd:string"
                },
                {
                        "parameter": "Device.Users.User.1.Password",
                        "value": "1",
                        "type": "xsd:string"
                },
                {
                        "parameter": "Device.Users.User.1.RemoteAccessCapable",
                        "value": "1",
                        "type": "xsd:boolean"
                },
                {
                        "parameter": "Device.Users.User.1.Username",
                        "value": "1",
                        "type": "xsd:string"
                },
                {
                        "parameter": "Device.DeviceInfo.SerialNumber",
                        "value": "0",
                        "type": "xsd:string"
                }
        ]
}
root@iopsys:~#
```

### Set
API to set value to specific object, object name must be provided in `path` parameter and value to be set in `value` option.

> Note 1: In usp namespace, set method creates a internal transaction before actually setting the value. After set operation it automatically commits the transaction.

> Note 2: In usp.raw namespace, set method requires a transaction to be created before calling the set method. Please refer to transaction APIs for more details.

```console
root@iopsys:/tmp# ubus call usp set '{"path":"Device.WiFi.SSID.[BSSID==\"00:22:07:ae:ee:03\"].SSID", "value":"test-2g"}'
{
        "status": true
}
root@iopsys:/tmp# ubus call usp.raw set '{"path":"Device.WiFi.SSID.[BSSID==\"00:22:07:ae:ee:03\"].SSID", "value":"test-2g", "transaction_id":12345}'
{
        "status": true
}
root@iopsys:~# ubus call usp get '{"path":"Device.WiFi.SSID.[BSSID==\"00:22:07:ae:ee:03\"].SSID"}'
{
        "SSID": [
                {
                        "SSID": "test-2g"
                }
        ]
}
```
- For more info on the `usp` ubus API see [link](./docs/api/usp.md#set)
- For more info on the `usp.raw` ubus API see [link](./docs/api/usp.raw.md#set)

### Operate
API to run operate/diagnostics commands as defined in TR-369

```console
root@iopsys:~# ubus call usp operate '{"path":"Device.IP.Diagnostics.", "action":"IPPing()","input":{"Host":"iopsys.eu"}}'
{
        "Results": [
                {
                        "path": "Device.IP.Diagnostics.IPPing",
                        "result": [
                                {
                                        "AverageResponseTime": 0,
                                        "AverageResponseTimeDetailed": 0,
                                        "FailureCount": 3,
                                        "MaximumResponseTime": 0,
                                        "MaximumResponseTimeDetailed": 0,
                                        "MinimumResponseTime": 9999,
                                        "MinimumResponseTimeDetailed": 999999999,
                                        "SuccessCount": 0
                                }
                        ]
                }
        ]
}

root@iopsys:~# ubus call usp.raw operate '{"path":"Device.IP.Diagnostics.", "action":"IPPing()","input":{"Host":"iopsys.eu"}}'
{
        "Results": [
                {
                        "path": "Device.IP.Diagnostics.IPPing",
                        "parameters": [
                                {
                                        "parameter": "AverageResponseTime",
                                        "value": "0",
                                        "type": "xsd:unsignedInt"
                                },
                                {
                                        "parameter": "AverageResponseTimeDetailed",
                                        "value": "0",
                                        "type": "xsd:unsignedInt"
                                },
                                {
                                        "parameter": "FailureCount",
                                        "value": "3",
                                        "type": "xsd:unsignedInt"
                                },
                                {
                }
        ]
}

root@iopsys:~# ubus call usp operate '{"path":"Device.IP.Interface.[Name==\"wan\"].", "action":"Reset()"}'
{
        "Results": [
                {
                        "path": "Device.IP.Interface.2.Reset",
                        "result": [
                                {

                                }
                        ]
                }
        ]
}

root@iopsys:~# ubus call usp.raw operate '{"path":"Device.IP.Interface.[Name==\"wan\"].", "action":"Reset()"}'
{
        "Results": [
                {
                        "path": "Device.IP.Interface.2.Reset",
                        "parameters": [

                        ]
                }
        ]
}
```
- For more info on the `usp` ubus API see [link](./docs/api/usp.md#operate)
- For more info on the `usp.raw` ubus API see [link](./docs/api/usp.raw.md#operate)

### Add object
API to add new objects in multi-instance object

> Note 1: In usp namespace, `add_object` method creates a internal transaction before actually setting the value. After set operation it automatically commits the transaction.
> Note 2: In usp.raw namespace, `add_object` method requires a transaction to be created before calling the set method. Please refer to transaction APIs for more details.

```console
root@iopsys:~# ubus call usp add_object '{"path":"Device.Users.User."}'
{
        "parameters": [
                {
                        "parameter": "Device.Users.User.",
                        "status": true,
                        "instance": "4"
                }
        ]
}
root@iopsys:~# ubus call usp.raw add_object '{"path":"Device.Users.User.", "transaction_id":12345}'
{
        "parameters": [
                {
                        "parameter": "Device.Users.User.",
                        "status": true,
                        "instance": "5"
                }
        ]
}
```
- For more info on the `usp` ubus API see [link](./docs/api/usp.md#add_object)
- For more info on the `usp.raw` ubus API see [link](./docs/api/usp.raw.md#add_object)

### Delete object
API to delete an existing object from multi-instance object

> Note 1: In usp namespace, `del_object` method creates a internal transaction before actually setting the value. After set operation it automatically commits the transaction.
> Note 2: In usp.raw namespace, `del_object` method requires a transaction to be created before calling the set method. Please refer to transaction APIs for more details.

```console
root@iopsys:/tmp# ubus call usp del_object '{"path":"Device.Users.User.4"}'
{
        "parameters": [
                {
                        "parameter": "Device.Users.User.4.",
                        "status": true
                }
        ]
}
root@iopsys:/tmp# ubus call usp.raw del_object '{"path":"Device.Users.User.3", "transaction_id": 12345}'
{
        "parameters": [
                {
                        "parameter": "Device.Users.User.3.",
                        "status": true
                }
        ]
}
```
- For more info on the `usp` ubus API see [link](./docs/api/usp.md#del_object)
- For more info on the `usp.raw` ubus API see [link](./docs/api/usp.raw.md#del_object)

### Object names
API to get the available list of object names.

```console
root@iopsys:~# ubus call usp object_names '{"path":"Device.DeviceInfo.SerialNumber"}'
{
        "parameters": [
                {
                        "parameter": "Device.DeviceInfo.SerialNumber",
                        "writable": "0",
                        "type": "xsd:string"
                }
        ]
}
root@iopsys:~#
root@iopsys:~# ubus call usp.raw object_names '{"path":"Device.Users.User.1."}'
{
        "parameters": [
                {
                        "parameter": "Device.Users.User.1.",
                        "writable": "1",
                        "type": "xsd:object"
                },
                {
                        "parameter": "Device.Users.User.1.Alias",
                        "writable": "1",
                        "type": "xsd:string"
                },
                {
                        "parameter": "Device.Users.User.1.Enable",
                        "writable": "1",
                        "type": "xsd:boolean"
                },
                {
                        "parameter": "Device.Users.User.1.Language",
                        "writable": "1",
                        "type": "xsd:string"
                },
                {
                        "parameter": "Device.Users.User.1.Password",
                        "writable": "1",
                        "type": "xsd:string"
                },
                {
                        "parameter": "Device.Users.User.1.RemoteAccessCapable",
                        "writable": "1",
                        "type": "xsd:boolean"
                },
                {
                        "parameter": "Device.Users.User.1.Username",
                        "writable": "1",
                        "type": "xsd:string"
                }
        ]
}
root@iopsys:~#
```

- For more info on the `usp` ubus API see [link](./docs/api/usp.md)
- For more info on the `usp.raw` ubus API see [link](./docs/api/usp.raw.md)

### Instances
API to get the available instances of an multi-instance object. USP Instances method returns the registered instances.

```console
root@iopsys:~# ubus call usp instances '{"path":"Device.IP.Interface.", "proto":"usp"}'
{
        "parameters": [
                {
                        "parameter": "Device.IP.Interface.1."
                },
                {
                        "parameter": "Device.IP.Interface.1.IPv4Address.1."
                },
                {
                        "parameter": "Device.IP.Interface.2."
                },
                {
                        "parameter": "Device.IP.Interface.3."
                },
                {
                        "parameter": "Device.IP.Interface.3.IPv4Address.1."
                },
                {
                        "parameter": "Device.IP.Interface.3.IPv6Address.1."
                },
                {
                        "parameter": "Device.IP.Interface.3.IPv6Prefix.1."
                }
        ]
}
```

- For more info on the `usp` ubus API see [link](./docs/api/usp.md#instances)
- For more info on the `usp.raw` ubus API see [link](./docs/api/usp.raw.md#instances)

### Validate
API to validate a object in data-model. This api shall simply return the object name
if present in data-model objects or generates a fault if object not available in
data-model.

```console
root@iopsys:~# ubus call usp validate '{"path":"Device.DeviceInfo."}'
{
        "parameter": "Device.DeviceInfo"
}
root@iopsys:~# ubus call usp.raw validate '{"path":"invalid.object"}'
{
        "fault": 9005
}
```

### List Operate
API to list all available operate commands with supported input and output parameters


```console
root@iopsys:~# ubus call usp list_operate
{
        "parameters": [
                {
                        "parameter": "Device.DHCPv4.Client.{i}.Renew()",
                        "type": "sync"
                },
                {
                        "parameter": "Device.DNS.Diagnostics.NSLookupDiagnostics()",
                        "type": "async",
                        "in": [
                                "HostName",
                                "Interface",
                                "DNSServer",
                                "Timeout",
                                "NumberOfRepetitions"
                        ],
                        "out": [
                                "Status",
                                "AnswerType",
                                "HostNameReturned",
                                "IPAddresses",
                                "DNSServerIP",
                                "ResponseTime"
                        ]
                },
                {
                        "parameter": "Device.DeviceInfo.FirmwareImage.{i}.Activate()",
                        "type": "async"
                },
                {
                        "parameter": "Device.IP.Diagnostics.IPPing()",
                        "type": "async",
                        "in": [
                                "Interface",
                                "ProtocolVersion",
                                "Host",
                                "NumberOfRepetitions",
                                "Timeout",
                                "DataBlockSize",
                                "DSCP"
                        ],
                        "out": [
                                "Status",
                                "IPAddressUsed",
                                "SuccessCount",
                                "FailureCount",
                                "AverageResponseTime",
                                "MinimumResponseTime",
                                "MaximumResponseTime",
                                "AverageResponseTimeDetailed",
                                "MinimumResponseTimeDetailed",
                                "MaximumResponseTimeDetailed"
                        ]
                },
                {
```

### Dump schema
API to dump all registered schema paths,

```console
root@iopsys:~# ubus call usp dump_schema
{
        "parameters": [
                {
                        "parameter": "Device.ATM.Link.{i}.",
                        "writable": "1",
                        "type": "xsd:object"
                },
                {
                        "parameter": "Device.ATM.Link.{i}.Alias",
                        "writable": "1",
                        "type": "xsd:string"
                },
                {
                        "parameter": "Device.ATM.Link.{i}.DestinationAddress",
                        "writable": "1",
                        "type": "xsd:string"
                },
                {
                        "parameter": "Device.ATM.Link.{i}.Enable",
                        "writable": "1",
                        "type": "xsd:boolean"
                },
                {
```

### Set multiple values
API to set value of multiple parameters at once.

> Note: This method only available in usp.raw namespace

```console
root@iopsys:~# ubus call usp.raw setm_values '{"pv_tuple":[{"path":"Device.Users.User.2.Username", "value":"xzzz"}, {"path":"Device.Users.User.2.RemoteAccessCapable", "value":"true"}, {"path":"Device.Users.User.2.Password", "value":"zzzzzzz"}], "proto":"usp", "transaction_id":1249743667}'
{
        "status": true
}
root@iopsys:~#
root@iopsys:~#
root@iopsys:~# ubus call usp.raw setm_values '{"pv_tuple":[{"path":"Device.Users.User.2.Username", "value":"xzzz"}, {"path":"Device.Users.User.2.RemoteAccessCapable", "value":"dummy"}, {"path":"Device.Users.User.2.Password", "value":"zzzzzzz"}], "proto":"usp", "transaction_id":738335779}'
{
        "parameters": [
                {
                        "path": "Device.Users.User.2.RemoteAccessCapable",
                        "status": false,
                        "fault": 7012
                }
        ]
}
```

### Transaction start
To support the `allow_partial` and `required` parameters in Add/Del/Set operation as defined in TR-369, transaction_* APIs introduced.
It basically works around data-model objects which has dependent uci config files for each CUD operation.
API to start a transaction in usp.raw namespace for add/del/set operations in usp.raw namespace

> Note: This API only available in usp.raw namespace

```console
root@iopsys:~# ubus call usp.raw transaction_start '{"app":"test"}'
{
        "status": true,
        "transaction_id": 955001092
}
```

### Get status of a transaction
API to get details and check status of a transaction id in usp.raw namespace

```console
root@iopsys:~# ubus call usp.raw transaction_status '{"transaction_id":955001092}'
{
        "app": "test",
        "status": "on-going",
        "remaining_time": 634
}
root@iopsys:~#
root@iopsys:~# ubus call usp.raw transaction_status '{"transaction_id":869066287}'
{
        "status": "not-exists"
}
root@iopsys:~#
```

### Transaction commit
API to commit an on-going transaction, on calling this api, uci changes shall
be committed and required services shall be restarted.

```console
root@iopsys:~# ubus call usp.raw transaction_commit '{"transaction_id":955001092}'
{
        "status": true
}
root@iopsys:~#
```

### Transaction abort
API to abort an on-going transaction, on calling this api, staged changes in
uci shall be reverted to earlier values.

```console
root@iopsys:~# ubus call usp.raw transaction_abort '{"transaction_id":955001092}'
{
        "status": true
}
root@iopsys:~#
```

### List supported usp events
API to list down the data-model events for usp notification supported by uspd.

```console
root@iopsys:~# ubus call usp.raw list_events
{
	"parameters": [
		{
			"parameter": "Device.LocalAgent.TransferComplete!",
			"in": [
				"Command",
				"CommandKey",
				"Requestor",
				"TransferType",
				"Affected",
				"TransferURL",
				"StartTime",
				"CompleteTime",
				"FaultCode",
				"FaultString"
			]
		}
	]
}
```

## Path syntax and possible error cases
Please note some error scenerios with the uspd.

1. The path parameter value must start with 'Device.'. The command below doesn't have Device before path "Users.User."

```console
root@iopsys:~# ubus call usp.raw get '{"path":"Users.User."}'
{
        "fault": 7026
}
```

2. The path parameter must end with a '.' if the path element is not a leaf element e.g.,
   Note that first two commands doesn't end with a '.' while the command with Alias is correct, due to Alias
   being the leaf element. To find correct schema path user can check with dump_schema option.

```console
root@iopsys:~#
root@iopsys:~# ubus call usp get '{"path":"Device.Users.User.4"}'
{
        "fault": 7026
}
root@iopsys:~#
root@iopsys:~# ubus call usp get '{"path":"Device.Users.User"}'
{
        "fault": 9005
}
root@iopsys:~#
root@iopsys:~# ubus call usp get '{"path":"Device.Users.User.*"}'
{
        "fault": 7026
}
root@iopsys:~#
root@iopsys:~# ubus call usp get '{"path":"Device.Users.User.4.Alias"}'
{
        "Alias": "cpe-4"
}
```

3. In path parameter value below, note that, the first search expression 'Type==Normal' is string which should be used as : Type==\"Normal\"

```console
root@iopsys:~# ubus call usp get '{"path":"Device.IP.Interface.[Type==Normal].IPv4Address.[AddressingType==\"Static\"].IPAddress"}'
{
        "fault": 7008
}
root@iopsys:~#
root@iopsys:~# ubus call usp get '{"path":"Device.IP.Interface.[Type==\"Normal\"].IPv4Address.[AddressingType==\"Static\"].IPAddress"}'
{
        "Interface": [
                {
                        "IPv4Address": [
                                {
                                        "IPAddress": "2.0.0.3"
                                }
                        ]
                }
        ]
}
```

4. The path parameter value must not have an empty search expression

```console
root@iopsys:~# ubus call usp get '{"path":"Device.Users.User.[]."}'
{
        "fault": 9005
}
```

5. The path parameter value must use proper '.' separated path search expression. Note that a '.' is missing between User and *

```console
root@iopsys:~# ubus call usp get '{"path":"Device.Users.User*."}'
{
        "fault": 7026
}
```

6. The path parameter value must be a valid path schema, in example below SSID is used which is invalid schema element for Device.Users.User.{i}.

```console
root@iopsys:~# ubus call usp get '{"path":"Device.Users.User.1.SSID"}'
{
        "fault": 7026
}
```

7. Please note that in search expression, string comparison only work with "==" or "!=". Whereas in command below its =

```console
root@iopsys:~# ubus call usp get '{"path":"Device.Users.User.[Username=\"user\"].Alias"}'
{
        "fault": 7008
}
```

#### Errors Codes

| Error Code | Meaning                                                      |
|------------|--------------------------------------------------------------|
| 7003       | Message failed due to an internal error.                     |
| 7004       | Message failed due to invalid values in the request elements and/or failure to update one or more parameters during Add or Set requests. |
| 7005       | Message failed due to memory or processing limitations.      |
| 7008       | Requested path was invalid or a reference was invalid.       |
| 7010       | Requested Path Name associated with this ParamError did not match any instantiated parameters. |
| 7011       | Unable to convert string value to correct data type.         |
| 7012       | Out of range or invalid enumeration.                         |
| 7022       | Command failed to operate.                                   |
| 7026       | Path is not present in the data model schema.                |


## Parallel calls
All `operate` operation and `get` operation with a depth up to 'USP_SUBPROCESS_DEPTH(2)'
runs in a parallel subprocess to avoid blocking the next call.

```console
root@iopsys:~# time ubus call usp.raw get '{"path":"Device."}' >/dev/null &
root@iopsys:~# time ubus call usp.raw get '{"path":"Device.Users."}' >/dev/null
real    0m 0.07s
user    0m 0.00s
sys     0m 0.00s
root@iopsys:~#
real     0m 1.86s
user    0m 0.05s
sys     0m 0.00s

[1]+  Done                       time ubus call usp.raw get "{\"path\":\"Device.\"}" >/dev/null
root@iopsys:~#
```

## More example(s)

```console

root@iopsys:~# ubus call usp get '{"path":"Device.WiFi.SSID.*.SSID"}'
{
        "SSID": [
                {
                        "SSID": "NORRLAND-34E380760120"
                },
                {
                        "SSID": "NORRLAND-34E380760120"
                }
        ]
}

root@iopsys:~# ubus call usp get '{"path":"Device.WiFi.SSID.*.BSSID"}'
{
        "SSID": [
                {
                        "BSSID": "34:E3:80:76:01:22"
                },
                {
                        "BSSID": "34:E3:80:76:01:23"
                }
        ]
}

root@iopsys:~# ubus call usp get '{"path":"Device.WiFi.SSID.[BSSID==\"34:E3:80:76:01:22\"].SSID"}'
{
        "SSID": [
                {
                        "SSID": "NORRLAND-34E380760120"
                }
        ]
}

root@iopsys:~# ubus call usp get '{"path":"Device.IP.Interface.[Status==\"Up\"].IPv4Address.[AddressingType==\"DHCP\"].IPAddress"}'
{
        "Interface": [
                {
                        "IPv4Address": [
                                {
                                        "IPAddress": "192.168.0.96"
                                }
                        ]
                }
        ]
}

root@iopsys:~# ubus call usp get '{"path":"Device.IP.Interface.[Status==\"Up\"].IPv4Address.[AddressingType==\"DHCP\"&&Status==\"Up\"]."}'
{
        "Interface": [
                {
                        "IPv4Address": [
                                {
                                        "AddressingType": "DHCP",
                                        "Alias": "cpe-2",
                                        "Enable": true,
                                        "IPAddress": "192.168.0.96",
                                        "Status": "Up",
                                        "SubnetMask": "255.255.255.0",
                                        "X_IOPSYS_EU_FirewallEnabled": true
                                }
                        ]
                }
        ]
}

root@iopsys:~# ubus call usp get '{"path":"Device.IP.Interface.[Type==\"Normal\"&&Stats.PacketsSent<=500].IPv4Address.[AddressingType==\"Static\"].IPAddress"}'
{
        "Interface": [
                {
                        "IPv4Address": [
                                {
                                        "IPAddress": "192.168.1.1"
                                }
                        ]
                }
        ]
}

root@iopsys:~# ubus call usp get '{"path":"Device.WiFi.AccessPoint.[SSIDReference+.SSID==\"NORRLAND-34E380760120\"].AssociatedDevice.[Noise>15].SignalStrength"}
'
{
        "AccessPoint": [
                {
                        "AssociatedDevice": [
                                {
                                        "SignalStrength": -31
                                }
                        ]
                }
        ]
}


root@iopsys:~# ubus call usp get '{"path":"Device.WiFi.SSID.*.LowerLayers#1+.Name"}'
{
        {
                "Name": "wlan0",
                "Name": "wlan2"
        }
}



root@iopsys:~# ubus call usp get '{"path":"Device.Users.User.*.Username"}'
{
        "User": [
                {
                        "Username": "user"
                },
                {
                        "Username": "support"
                },
                {
                        "Username": "admin"
                }
        ]
}

root@iopsys:~# ubus call usp.raw set '{"path":"Device.IP.Diagnostics.IPPing.DiagnosticsState", "value":"Requested", "proto":"cwmp"}'
{
        "parameters": [
                {
                        "path": "Device.IP.Diagnostics.IPPing.DiagnosticsState",
                        "status": true,
                        "flag": 16
                }
        ]
}

root@iopsys:~# ubus call usp.raw set '{"path":"Device.Users.User.2.Username", "value":"abc", "proto":"cwmp"}'
{
        "parameters": [
                {
                        "path": "Device.Users.User.2.Username",
                        "status": true,
                        "flag": 0
                }
        ]
}

root@iopsys:~# ubus call usp.raw set '{"path":"Device.Users.User.2.Username", "value":"abc", "proto":"usp"}'
{
        "parameters": [
                {
                        "path": "Device.Users.User.2.Username",
                        "status": true
                }
        ]
}

root@iopsys:~# ubus call usp.raw set '{"path":"Device.Users.User.2.Username", "value":"abc"}'
{
        "parameters": [
                {
                        "path": "Device.Users.User.2.Username",
                        "status": true
                }
        ]
}

root@iopsys:~#
root@iopsys:~# ubus call usp set '{"path":"Device.Users.User.[Username==\"xyz1\"].", "values":{"Username":"xyz1", "Enable":"dummy", "Password":"yzssssx"}, "proto":"usp"}'
{
        "parameters": [
                {
                        "path": "Device.Users.User.2.Username",
                        "status": true
                },
                {
                        "path": "Device.Users.User.2.Enable",
                        "status": false,
                        "fault": 7012
                },
                {
                        "path": "Device.Users.User.2.Password",
                        "status": true
                }
        ]
}
root@iopsys:~#
root@iopsys:~# ubus call usp.raw setm_values '{"pv_tuple":[{"path":"Device.Users.User.2.Username", "value":"xzzz"}, {"path":"Device.Users.User.2.RemoteAccessCapable", "value":"1"}, {"path":"Device.Users.User.2.Password", "value":"zzzzzzz"}], "proto":"usp"}'
{
        "parameters": [
                {
                        "path": "Device.Users.User.2.Username",
                        "status": true
                },
                {
                        "path": "Device.Users.User.2.RemoteAccessCapable",
                        "status": false,
                        "fault": 7012
                },
                {
                        "path": "Device.Users.User.2.Password",
                        "status": true
                }
        ]
}
```

- For more info on the usp ubus API see [link](./docs/api/usp.md)
- For more info on the usp.raw ubus API see [link](./docs/api/usp.raw.md)


## Concepts and Workflow

`uspd` internally uses `libbbfdm` to get the data-model objects. On startup it parses the uci file to check if the granularity is set and then as per the granularity value it registers the required ubus namespaces.

When a ubus method is called it first checks the `path` parameter to identify if it has special USP syntax, if present it parses and determine the correct objects from `libbbfdm`, then proceeds with the `Get/Set/Operate/Add/Del` operation on the qualified objects.

So, `uspd` search for `[[+*]+` in path expression, if it matches then segments the path and get the schema from `libbbfdm` and store it in a link-list, then it proceeds with the next segment to filter out the unneeded schema paths. It keeps on doing so till all the expressions are solved and it finally left with qualified objects.
Once all the expressions are solved, it starts getting the values for qualified objects and store it in a `stack` to print the output in pretty format.

For operate command, it solve the path expression and then call `bbf_operate` from `libbbfdm` to execute the operate command.

`uspd` uses `dm_entry_param_method` API from `libbbfdm` to get the device tree schema and it's values.

In short, it covers/supports the new syntax introduced in `TR-369` by using the existing data-model available with `libbbfdm`.

## Dependencies

### Build-Time Dependencies

To successfully build uspd, following libraries are needed:

| Dependency |                    Link                     |    License     |
| ---------- | ------------------------------------------- | -------------- |
| libuci     | https://git.openwrt.org/project/uci.git     | LGPL 2.1       |
| libubox    | https://git.openwrt.org/project/libubox.git | BSD            |
| libubus    | https://git.openwrt.org/project/ubus.git    | LGPL 2.1       |
| libjson-c  | https://s3.amazonaws.com/json-c_releases    | MIT            |
| libbbfdm   | https://dev.iopsys.eu/iopsys/bbf.git        | LGPL 2.1       |


### Run-Time Dependencies

In order to run the `uspd`, following dependencies are needed to be running/available before `uspd`.

| Dependency |                   Link                   | License  |
| ---------- | ---------------------------------------- | -------- |
| ubusd      | https://git.openwrt.org/project/ubus.git | LGPL 2.1 |
| libbbfdm   | https://dev.iopsys.eu/iopsys/bbf.git     | LGPL 2.1 |

System daemon `ubusd` is used to expose the USP functionality over `ubus`.
