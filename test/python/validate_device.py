#!/usr/bin/python3

import subprocess
import json

TEST_NAME = "Get Device."

print("Running: " + TEST_NAME)

out = subprocess.Popen(['ubus', 'call', 'usp', 'get', '{"path":"Device."}'], 
           stdout=subprocess.PIPE, 
           stderr=subprocess.STDOUT)

stdout,stderr = out.communicate()

jout = json.loads(stdout)
assert isinstance(jout, dict), "FAIL: get Device. on usp"

out = subprocess.Popen(['ubus', 'call', 'usp.raw', 'get', '{"path":"Device."}'], 
           stdout=subprocess.PIPE, 
           stderr=subprocess.STDOUT)

stdout,stderr = out.communicate()

jout = json.loads(stdout)
assert isinstance(jout, dict), "FAIL: get Device. on usp.raw"

print("PASS: " + TEST_NAME)
