#!/usr/bin/python3

import ubus
import json
import pathlib

TEST_NAME = "Validate ubus event usp.raw"

print("Running: " + TEST_NAME)

def callback(event, data):
    print("PASS: " + TEST_NAME)
    ubus.disconnect()
    exit(0)
    
sock = pathlib.Path('/var/run/ubus/ubus.sock')
if sock.exists ():
    assert ubus.connect('/var/run/ubus/ubus.sock')
else:
    assert ubus.connect()

ubus.listen(("usp.event", callback))

ubus.call("usp.raw", "notify_event", {"name":"Device.LocalAgent.TransferComplete!", "input":{"param1":"val1", "param2":"val2"}})

ubus.loop()

ubus.disconnect()

print("FAIL: " + TEST_NAME)
