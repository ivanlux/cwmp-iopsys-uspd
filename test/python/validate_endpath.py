#!/usr/bin/python3

import subprocess
import json

TEST_NAME = "Get upstream DSL path"

print("Running: " + TEST_NAME)

out = subprocess.Popen(['ubus', 'call', 'usp.raw', 'get', '{"path":"Device.DSL.Line.*.Upstream"}'], 
           stdout=subprocess.PIPE, 
           stderr=subprocess.STDOUT)

stdout,stderr = out.communicate()

jout = json.loads(stdout)

assert len(jout["parameters"]) == 1, "FAIL: got more parameters" + jout["parameters"]

print("PASS: " + TEST_NAME)
