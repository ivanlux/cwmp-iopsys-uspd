#ifndef TEST_UTILS_H
#define TEST_UTILS_H

#define GET_PATH "Device.DeviceInfo.Manufacturer"
#define GET_PROTO "usp"
#define GET_RAW_PROTO "usp.raw"
#define GET_CWMP_PROTO "cwmp"

#endif
