#!/bin/bash

rm -f top.log
pid=`supervisorctl pid uspd`
if [ $? != 0 ]; then
    echo "FAILED TO GET PID."
    exit 1
fi

while :
do
    top -p $pid -b -n 1 |sed 1,7d >> ./top.log
done

