#!/bin/bash

echo "install dependencies for unit-test script"
pwd

source ./gitlab-ci/shared.sh

# install required packages
exec_cmd apt update
exec_cmd apt install -y python3-pip
exec_cmd pip3 install pexpect ubus

# install libbbf
cd /opt/dev
rm -rf bbf
exec_cmd git clone -b devel https://dev.iopsys.eu/iopsys/bbf.git
cd /opt/dev/bbf

echo "BBF Upstream Hash ${UPSTREAM_BBF_SHA}"
if [ -n "${UPSTREAM_BBF_SHA}" ]; then
	exec_cmd git checkout ${UPSTREAM_BBF_SHA}
fi
git log -1

source ./gitlab-ci/shared.sh
install_libbbf
