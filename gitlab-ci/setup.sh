#!/bin/bash

echo "preparation script"

pwd

mkdir -p /etc/bbfdm/
cp -r ./test/files/etc/* /etc/
cp -r ./test/files/usr/* /usr/
cp -r ./test/files/tmp/* /tmp/
cp ./gitlab-ci/iopsys-supervisord.conf /etc/supervisor/conf.d/

ls /etc/config/
ls /usr/share/rpcd/schemas/
ls /etc/supervisor/conf.d/
