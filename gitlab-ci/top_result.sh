#!/bin/bash

pwd

highest_mem_usage () {
	val=$(cat ../top.log |awk '{printf $10 "\n"}' |sort -nr|head -n 1)
	echo "Max memory usage during test is :: ${val}"
}

highest_cpu_usage () {
	val=$(cat ../top.log |awk '{printf $9 "\n"}' |sort -nr|head -n 1)
	echo "Max CPU usage during test is :: ${val}"
}

highest_cpu_usage
highest_mem_usage

