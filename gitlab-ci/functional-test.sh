#!/bin/bash

echo "$0 preparation script"
pwd

source ./gitlab-ci/shared.sh

trap cleanup EXIT
trap cleanup SIGINT

make clean
exec_cmd make func-test -C ./

supervisorctl status all
supervisorctl update
supervisorctl restart all
exec_cmd ubus wait_for usp.raw usp
supervisorctl status all

# debug logging
echo "Checking ubus status [$(date '+%d/%m/%Y %H:%M:%S')]"
ubus list
ubus -v list usp.raw
ubus -v list usp

echo "Checking system resources"
free -h
df -h

echo "## Running python based verification of functionalities ##"
echo > ./funl-result.log
num=0
for test in `ls -1 ./test/python/*.py`
do
	num=$(( num + 1 ))
	$test
	if [ $? -eq 0 ]; then
		echo "ok ${num} - $test" >> ./funl-result.log

	else
		echo "not ok ${num} - $test" >>  ./funl-result.log
	fi
done

echo "1..${num}" >> ./funl-result.log

# run functional on usp object validation
if [ -f "/usr/share/rpcd/schemas/usp*.json" ]; then
	rm /usr/share/rpcd/schemas/usp*.json
fi

# run functional on usp object validation
cp -r ./schemas/ubus/usp.json /usr/share/rpcd/schemas
ubus-api-validator -f ./test/funl/json/usp.validation.json >> ./funl-result.log
ubus-api-validator -f ./test/funl/json/usp.mandatoryParam.json >> ./funl-result.log
ret=$?

# run functional on usp object validation
rm /usr/share/rpcd/schemas/usp*.json
cp -r ./schemas/ubus/usp.raw.json /usr/share/rpcd/schemas
ubus-api-validator -f ./test/funl/json/usp.raw.validation.json >> ./funl-result.log
ubus-api-validator -f ./test/funl/json/usp.raw.mandatoryParam.json >> ./funl-result.log
ret=$?

# TODO: add for granularity ubus objects
#uci set uspd.usp.granularitylevel='1'
#uci commit

#ubus-api-validator -f ./test/funl/json/gran/gran.validation.json >> ./funl-result.log
#ret=$?

#uci set uspd.usp.granularitylevel='0'
#uci commit

supervisorctl stop all
supervisorctl status

#report part
gcovr -r . --xml -o ./funl-test-coverage.xml
gcovr -r .
date +%s > timestamp.log
cp ./memory-report.xml ./funl-test-memory-report.xml
exec_cmd tap-junit --input ./funl-result.log --output report

echo "Checking memory leaks..."
grep -q "Leak_DefinitelyLost" memory-report.xml
error_on_zero $?

echo "Functional Test :: PASS"
