#!/bin/bash

echo "$0 preparation script"
pwd

source ./gitlab-ci/shared.sh

trap cleanup EXIT
trap cleanup SIGINT

# clean and make
make clean
exec_cmd make func-test -C ./

supervisorctl status all
supervisorctl update
supervisorctl restart all
exec_cmd ubus -t 10 wait_for usp.raw usp
supervisorctl status all

# debug logging
echo "Checking ubus status [$(date '+%d/%m/%Y %H:%M:%S')]"
ubus list
ubus -v list usp.raw
ubus -v list usp

echo "Checking system resources"
free -h
df -h

# run functional on usp object validation
if [ -f "/usr/share/rpcd/schemas/usp*.json" ]; then
	rm /usr/share/rpcd/schemas/usp*.json
fi

cp -r ./schemas/ubus/usp.json /usr/share/rpcd/schemas
ubus-api-validator -f ./test/api/json/usp.validation.json > ./api-result.log
check_ret $?

# run functional on usp object validation
if [ -f "/usr/share/rpcd/schemas/usp*.json" ]; then
	rm /usr/share/rpcd/schemas/usp*.json
fi

cp -r ./schemas/ubus/usp.raw.json /usr/share/rpcd/schemas
ubus-api-validator -f ./test/api/json/usp.raw.validation.json >> ./api-result.log
check_ret $?

supervisorctl status all
supervisorctl stop all
supervisorctl status

#report part
date +%s > timestamp.log
gcovr -r . --xml -o ./api-test-coverage.xml
gcovr -r .
cp ./memory-report.xml ./api-test-memory-report.xml
exec_cmd tap-junit --input ./api-result.log --output report

echo "Checking memory leaks..."
grep -q "Leak_DefinitelyLost" memory-report.xml
error_on_zero $?

echo "Functional ubus API test :: PASS"
