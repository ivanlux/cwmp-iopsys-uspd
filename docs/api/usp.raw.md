# usp.raw Schema

```
https://www.iopsys.eu/usp.raw.json
```

| Custom Properties | Additional Properties |
| ----------------- | --------------------- |
| Forbidden         | Forbidden             |

# usp.raw

| List of Methods                           |
| ----------------------------------------- |
| [add_object](#add_object)                 | Method | usp.raw (this schema) |
| [del_object](#del_object)                 | Method | usp.raw (this schema) |
| [dump_schema](#dump_schema)               | Method | usp.raw (this schema) |
| [get](#get)                               | Method | usp.raw (this schema) |
| [getm_names](#getm_names)                 | Method | usp.raw (this schema) |
| [getm_values](#getm_values)               | Method | usp.raw (this schema) |
| [instances](#instances)                   | Method | usp.raw (this schema) |
| [list_events](#list_events)               | Method | usp.raw (this schema) |
| [list_operate](#list_operate)             | Method | usp.raw (this schema) |
| [object_names](#object_names)             | Method | usp.raw (this schema) |
| [operate](#operate)                       | Method | usp.raw (this schema) |
| [set](#set)                               | Method | usp.raw (this schema) |
| [setm_values](#setm_values)               | Method | usp.raw (this schema) |
| [transaction_abort](#transaction_abort)   | Method | usp.raw (this schema) |
| [transaction_commit](#transaction_commit) | Method | usp.raw (this schema) |
| [transaction_start](#transaction_start)   | Method | usp.raw (this schema) |
| [validate](#validate)                     | Method | usp.raw (this schema) |

## add_object

### Add a new object instance

Add a new object in multi instance object

`add_object`

- type: `Method`

### add_object Type

`object` with following properties:

| Property | Type   | Required     |
| -------- | ------ | ------------ |
| `input`  | object | **Required** |
| `output` | object | Optional     |

#### input

`input`

- is **required**
- type: `object`

##### input Type

`object` with following properties:

| Property         | Type    | Required     | Default  |
| ---------------- | ------- | ------------ | -------- |
| `instance_mode`  | integer | Optional     |          |
| `key`            | string  | Optional     |          |
| `path`           | string  | **Required** |          |
| `proto`          | string  | Optional     | `"both"` |
| `transaction_id` | integer | **Required** |          |

#### instance_mode

`instance_mode`

- is optional
- type: `integer`

##### instance_mode Type

`integer`

- minimum value: `0`
- maximum value: `1`

#### key

`key`

- is optional
- type: `string`

##### key Type

`string`

#### path

Complete object element path as per TR181

`path`

- is **required**
- type: reference

##### path Type

`string`

- minimum length: 6 characters
- maximum length: 256 characters

##### path Examples

```json
Device.
```

```json
Device.DeviceInfo.Manufacturer
```

```json
Device.WiFi.SSID.1.
```

```json
Device.WiFi.
```

#### proto

`proto`

- is optional
- type: reference
- default: `"both"`

##### proto Type

`string`

The value of this property **must** be equal to one of the [known values below](#add_object-known-values).

##### proto Known Values

| Value |
| ----- |
| usp   |
| cwmp  |
| both  |

#### transaction_id

`transaction_id`

- is **required**
- type: `integer`

##### transaction_id Type

`integer`

- minimum value: `1`

### Ubus CLI Example

```
ubus call usp.raw add_object {"path":"esse irure sit","transaction_id":27603940,"proto":"cwmp","key":"ad dolore","instance_mode":1}
```

### JSONRPC Example

```json
{
  "jsonrpc": "2.0",
  "id": 0,
  "method": "call",
  "params": [
    "<SID>",
    "usp.raw",
    "add_object",
    { "path": "esse irure sit", "transaction_id": 27603940, "proto": "cwmp", "key": "ad dolore", "instance_mode": 1 }
  ]
}
```

#### output

`output`

- is optional
- type: `object`

##### output Type

`object` with following properties:

| Property   | Type    | Required |
| ---------- | ------- | -------- |
| `fault`    | integer | Optional |
| `instance` | integer | Optional |
| `status`   | boolean | Optional |

#### fault

`fault`

- is optional
- type: reference

##### fault Type

`integer`

- minimum value: `7000`
- maximum value: `9050`

#### instance

`instance`

- is optional
- type: `integer`

##### instance Type

`integer`

- minimum value: `1`

#### status

`status`

- is optional
- type: `boolean`

##### status Type

`boolean`

### Output Example

```json
{ "status": true, "instance": 71042098, "fault": 8418 }
```

## del_object

### Delete object instance

Delete a object instance from multi instance object

`del_object`

- type: `Method`

### del_object Type

`object` with following properties:

| Property | Type   | Required     |
| -------- | ------ | ------------ |
| `input`  | object | **Required** |
| `output` | object | Optional     |

#### input

`input`

- is **required**
- type: `object`

##### input Type

`object` with following properties:

| Property         | Type    | Required     | Default  |
| ---------------- | ------- | ------------ | -------- |
| `instance_mode`  | integer | Optional     |          |
| `path`           | string  | **Required** |          |
| `proto`          | string  | Optional     | `"both"` |
| `transaction_id` | integer | **Required** |          |

#### instance_mode

`instance_mode`

- is optional
- type: `integer`

##### instance_mode Type

`integer`

- minimum value: `0`
- maximum value: `1`

#### path

DM object path with search queries

`path`

- is **required**
- type: reference

##### path Type

`string`

- minimum length: 6 characters
- maximum length: 256 characters

##### path Examples

```json
Device.
```

```json
Device.DeviceInfo.Manufacturer
```

```json
Device.WiFi.SSID.[SSID=="test_ssid"].BSSID
```

```json
Device.WiFi.SSID.*.BSSID
```

```json
Device.WiFi.SSID.[SSID!="test_ssid"&&Enable==1].BSSID
```

```json
Device.WiFi.
```

#### proto

`proto`

- is optional
- type: reference
- default: `"both"`

##### proto Type

`string`

The value of this property **must** be equal to one of the [known values below](#del_object-known-values).

##### proto Known Values

| Value |
| ----- |
| usp   |
| cwmp  |
| both  |

#### transaction_id

`transaction_id`

- is **required**
- type: `integer`

##### transaction_id Type

`integer`

- minimum value: `1`

### Ubus CLI Example

```
ubus call usp.raw del_object {"path":"eavelit qui Excepteur laboris","transaction_id":18825047,"proto":"cwmp","instance_mode":0}
```

### JSONRPC Example

```json
{
  "jsonrpc": "2.0",
  "id": 0,
  "method": "call",
  "params": [
    "<SID>",
    "usp.raw",
    "del_object",
    { "path": "eavelit qui Excepteur laboris", "transaction_id": 18825047, "proto": "cwmp", "instance_mode": 0 }
  ]
}
```

#### output

`output`

- is optional
- type: `object`

##### output Type

`object` with following properties:

| Property | Type    | Required |
| -------- | ------- | -------- |
| `fault`  | integer | Optional |
| `status` | boolean | Optional |

#### fault

`fault`

- is optional
- type: reference

##### fault Type

`integer`

- minimum value: `7000`
- maximum value: `9050`

#### status

`status`

- is optional
- type: `boolean`

##### status Type

`boolean`

### Output Example

```json
{ "status": false, "fault": 7284 }
```

## dump_schema

### Get available datamodel schema from Device

Schema will have all the nodes/objects supported by libbbf

`dump_schema`

- type: `Method`

### dump_schema Type

`object` with following properties:

| Property | Type   | Required     |
| -------- | ------ | ------------ |
| `input`  | object | **Required** |
| `output` | object | **Required** |

#### input

`input`

- is **required**
- type: `object`

##### input Type

`object` with following properties:

| Property | Type | Required |
| -------- | ---- | -------- |
| None     | None | None     |

### Ubus CLI Example

```
ubus call usp.raw dump_schema {}
```

### JSONRPC Example

```json
{ "jsonrpc": "2.0", "id": 0, "method": "call", "params": ["<SID>", "usp.raw", "dump_schema", {}] }
```

#### output

`output`

- is **required**
- type: `object`

##### output Type

`object` with following properties:

| Property     | Type  | Required     |
| ------------ | ----- | ------------ |
| `parameters` | array | **Required** |

#### parameters

`parameters`

- is **required**
- type: `array`

##### parameters Type

Array type: `array`

All items must be of the type: Unknown type ``.

```json
{
  "type": "array",
  "uniqueItems": true,
  "items": [
    {
      "type": "object",
      "properties": {
        "parameter": {
          "$ref": "#/definitions/schema_path_t"
        },
        "writable": {
          "$ref": "#/definitions/boolean_t"
        },
        "type": {
          "$ref": "#/definitions/type_t"
        },
        "unique_keys": {
          "type": "array",
          "uniqueItems": true,
          "maxItems": 8,
          "items": [
            {
              "type": "string"
            }
          ]
        }
      },
      "required": ["parameter", "writable", "type", "unique_keys"]
    },
    {
      "type": "object",
      "properties": {
        "parameter": {
          "$ref": "#/definitions/schema_path_t"
        },
        "writable": {
          "$ref": "#/definitions/boolean_t"
        },
        "type": {
          "$ref": "#/definitions/type_t"
        }
      },
      "required": ["parameter", "writable", "type"]
    }
  ],
  "simpletype": "`array`"
}
```

### Output Example

```json
{
  "parameters": [
    { "parameter": "reprehenderit culpa", "writable": "0", "type": "xsd:dateTime", "unique_keys": ["irure"] },
    { "parameter": "nisi tempor mollit", "writable": "1", "type": "xsd:hexBinary" }
  ]
}
```

## get

### Get handler

Query the datamodel object

`get`

- type: `Method`

### get Type

`object` with following properties:

| Property | Type   | Required     |
| -------- | ------ | ------------ |
| `input`  | object | **Required** |
| `output` | object | Optional     |

#### input

`input`

- is **required**
- type: `object`

##### input Type

`object` with following properties:

| Property | Type   | Required     | Default  |
| -------- | ------ | ------------ | -------- |
| `path`   | string | **Required** |          |
| `proto`  | string | Optional     | `"both"` |

#### path

DM object path with search queries

`path`

- is **required**
- type: reference

##### path Type

`string`

- minimum length: 6 characters
- maximum length: 256 characters

##### path Examples

```json
Device.
```

```json
Device.DeviceInfo.Manufacturer
```

```json
Device.WiFi.SSID.[SSID=="test_ssid"].BSSID
```

```json
Device.WiFi.SSID.*.BSSID
```

```json
Device.WiFi.SSID.[SSID!="test_ssid"&&Enable==1].BSSID
```

```json
Device.WiFi.
```

#### proto

`proto`

- is optional
- type: reference
- default: `"both"`

##### proto Type

`string`

The value of this property **must** be equal to one of the [known values below](#get-known-values).

##### proto Known Values

| Value |
| ----- |
| usp   |
| cwmp  |
| both  |

### Ubus CLI Example

```
ubus call usp.raw get {"path":"sunt nulla incididunt do","proto":"both"}
```

### JSONRPC Example

```json
{
  "jsonrpc": "2.0",
  "id": 0,
  "method": "call",
  "params": ["<SID>", "usp.raw", "get", { "path": "sunt nulla incididunt do", "proto": "both" }]
}
```

#### output

`output`

- is optional
- type: `object`

##### output Type

`object` with following properties:

| Property     | Type    | Required |
| ------------ | ------- | -------- |
| `fault`      | integer | Optional |
| `parameters` | array   | Optional |

#### fault

`fault`

- is optional
- type: reference

##### fault Type

`integer`

- minimum value: `7000`
- maximum value: `9050`

#### parameters

`parameters`

- is optional
- type: `array`

##### parameters Type

Array type: `array`

All items must be of the type: Unknown type ``.

```json
{
  "type": "array",
  "items": [
    {
      "type": "object",
      "properties": {
        "parameter": {
          "$ref": "#/definitions/path_t"
        },
        "value": {
          "type": "string"
        },
        "type": {
          "$ref": "#/definitions/type_t"
        }
      }
    }
  ],
  "simpletype": "`array`"
}
```

### Output Example

```json
{
  "fault": 7854,
  "parameters": [{ "parameter": "sunt Excepteur nisi", "value": "aliquip occaecat", "type": "xsd:int" }]
}
```

## getm_names

### Get multiple object names

Query multiple object names at once

`getm_names`

- type: `Method`

### getm_names Type

`object` with following properties:

| Property | Type   | Required     |
| -------- | ------ | ------------ |
| `input`  | object | **Required** |
| `output` | object | Optional     |

#### input

`input`

- is **required**
- type: `object`

##### input Type

`object` with following properties:

| Property     | Type    | Required | Default  |
| ------------ | ------- | -------- | -------- |
| `next-level` | boolean | Optional |          |
| `paths`      | array   | Optional |          |
| `proto`      | string  | Optional | `"both"` |

#### next-level

gets only next level objects if true

`next-level`

- is optional
- type: `boolean`

##### next-level Type

`boolean`

#### paths

`paths`

- is optional
- type: `array`

##### paths Type

Array type: `array`

All items must be of the type: Unknown type ``.

```json
{
  "type": "array",
  "items": [
    {
      "$ref": "#/definitions/path_t"
    }
  ],
  "simpletype": "`array`"
}
```

#### proto

`proto`

- is optional
- type: reference
- default: `"both"`

##### proto Type

`string`

The value of this property **must** be equal to one of the [known values below](#getm_names-known-values).

##### proto Known Values

| Value |
| ----- |
| usp   |
| cwmp  |
| both  |

### Ubus CLI Example

```
ubus call usp.raw getm_names {"paths":["nisi officia anim commodo "],"proto":"usp","next-level":true}
```

### JSONRPC Example

```json
{
  "jsonrpc": "2.0",
  "id": 0,
  "method": "call",
  "params": [
    "<SID>",
    "usp.raw",
    "getm_names",
    { "paths": ["nisi officia anim commodo "], "proto": "usp", "next-level": true }
  ]
}
```

#### output

`output`

- is optional
- type: `object`

##### output Type

`object` with following properties:

| Property     | Type  | Required     |
| ------------ | ----- | ------------ |
| `parameters` | array | **Required** |

#### parameters

`parameters`

- is **required**
- type: `array`

##### parameters Type

Array type: `array`

All items must be of the type: Unknown type ``.

```json
{
  "type": "array",
  "items": [
    {
      "type": "object",
      "properties": {
        "parameter": {
          "$ref": "#/definitions/path_t"
        },
        "value": {
          "$ref": "#/definitions/boolean_t"
        },
        "type": {
          "$ref": "#/definitions/type_t"
        }
      }
    }
  ],
  "simpletype": "`array`"
}
```

### Output Example

```json
{ "parameters": [{ "parameter": "Loremesse do labore dolor", "value": "1", "type": "xsd:unsignedInt" }] }
```

## getm_values

### Get multiple values

Query multiple paths at once

`getm_values`

- type: `Method`

### getm_values Type

`object` with following properties:

| Property | Type   | Required     |
| -------- | ------ | ------------ |
| `input`  | object | **Required** |
| `output` | object | Optional     |

#### input

`input`

- is **required**
- type: `object`

##### input Type

`object` with following properties:

| Property | Type   | Required | Default  |
| -------- | ------ | -------- | -------- |
| `paths`  | array  | Optional |          |
| `proto`  | string | Optional | `"both"` |

#### paths

`paths`

- is optional
- type: `array`

##### paths Type

Array type: `array`

All items must be of the type: Unknown type ``.

```json
{
  "type": "array",
  "items": [
    {
      "$ref": "#/definitions/path_t"
    }
  ],
  "simpletype": "`array`"
}
```

#### proto

`proto`

- is optional
- type: reference
- default: `"both"`

##### proto Type

`string`

The value of this property **must** be equal to one of the [known values below](#getm_values-known-values).

##### proto Known Values

| Value |
| ----- |
| usp   |
| cwmp  |
| both  |

### Ubus CLI Example

```
ubus call usp.raw getm_values {"paths":["sint anim non irure"],"proto":"usp"}
```

### JSONRPC Example

```json
{
  "jsonrpc": "2.0",
  "id": 0,
  "method": "call",
  "params": ["<SID>", "usp.raw", "getm_values", { "paths": ["sint anim non irure"], "proto": "usp" }]
}
```

#### output

`output`

- is optional
- type: `object`

##### output Type

`object` with following properties:

| Property     | Type  | Required     |
| ------------ | ----- | ------------ |
| `parameters` | array | **Required** |

#### parameters

`parameters`

- is **required**
- type: `array`

##### parameters Type

Array type: `array`

All items must be of the type: Unknown type ``.

```json
{
  "type": "array",
  "items": [
    {
      "type": "object",
      "properties": {
        "parameter": {
          "$ref": "#/definitions/path_t"
        },
        "value": {
          "type": "string"
        },
        "type": {
          "$ref": "#/definitions/type_t"
        }
      }
    }
  ],
  "simpletype": "`array`"
}
```

### Output Example

```json
{
  "parameters": [
    { "parameter": "proident pariatur magna in Excepteur", "value": "qui in cupidatat", "type": "xsd:boolean" }
  ]
}
```

## instances

### Instance query handler

Get the instances of multi object

`instances`

- type: `Method`

### instances Type

`object` with following properties:

| Property | Type   | Required     |
| -------- | ------ | ------------ |
| `input`  | object | **Required** |
| `output` | object | Optional     |

#### input

`input`

- is **required**
- type: `object`

##### input Type

`object` with following properties:

| Property | Type   | Required     | Default  |
| -------- | ------ | ------------ | -------- |
| `path`   | string | **Required** |          |
| `proto`  | string | Optional     | `"both"` |

#### path

DM object path with search queries

`path`

- is **required**
- type: reference

##### path Type

`string`

- minimum length: 6 characters
- maximum length: 256 characters

##### path Examples

```json
Device.
```

```json
Device.DeviceInfo.Manufacturer
```

```json
Device.WiFi.SSID.[SSID=="test_ssid"].BSSID
```

```json
Device.WiFi.SSID.*.BSSID
```

```json
Device.WiFi.SSID.[SSID!="test_ssid"&&Enable==1].BSSID
```

```json
Device.WiFi.
```

#### proto

`proto`

- is optional
- type: reference
- default: `"both"`

##### proto Type

`string`

The value of this property **must** be equal to one of the [known values below](#instances-known-values).

##### proto Known Values

| Value |
| ----- |
| usp   |
| cwmp  |
| both  |

### Ubus CLI Example

```
ubus call usp.raw instances {"path":"nostrud aliqua sunt consectetur dolore","proto":"both"}
```

### JSONRPC Example

```json
{
  "jsonrpc": "2.0",
  "id": 0,
  "method": "call",
  "params": ["<SID>", "usp.raw", "instances", { "path": "nostrud aliqua sunt consectetur dolore", "proto": "both" }]
}
```

#### output

`output`

- is optional
- type: `object`

##### output Type

`object` with following properties:

| Property     | Type  | Required     |
| ------------ | ----- | ------------ |
| `parameters` | array | **Required** |

#### parameters

`parameters`

- is **required**
- type: `array`

##### parameters Type

Array type: `array`

All items must be of the type: Unknown type ``.

```json
{
  "type": "array",
  "items": [
    {
      "type": "object",
      "properties": {
        "parameter": {
          "$ref": "#/definitions/instance_t"
        }
      }
    }
  ],
  "simpletype": "`array`"
}
```

### Output Example

```json
{ "parameters": [{ "parameter": "nostrud p" }] }
```

## list_events

### List down supported usp events

events will be shown in schema format

`list_events`

- type: `Method`

### list_events Type

`object` with following properties:

| Property | Type   | Required |
| -------- | ------ | -------- |
| `input`  | object | Optional |
| `output` | object | Optional |

#### input

`input`

- is optional
- type: `object`

##### input Type

`object` with following properties:

| Property | Type | Required |
| -------- | ---- | -------- |
| None     | None | None     |

### Ubus CLI Example

```
ubus call usp.raw list_events {}
```

### JSONRPC Example

```json
{ "jsonrpc": "2.0", "id": 0, "method": "call", "params": ["<SID>", "usp.raw", "list_events", {}] }
```

#### output

`output`

- is optional
- type: `object`

##### output Type

`object` with following properties:

| Property     | Type  | Required     |
| ------------ | ----- | ------------ |
| `parameters` | array | **Required** |

#### parameters

`parameters`

- is **required**
- type: `array`

##### parameters Type

Array type: `array`

All items must be of the type: Unknown type ``.

```json
{
  "type": "array",
  "items": [
    {
      "type": "object",
      "required": ["parameter", "type"],
      "properties": {
        "parameter": {
          "$ref": "#/definitions/operate_path_t"
        },
        "in": {
          "type": "array",
          "items": [
            {
              "type": "string"
            }
          ]
        }
      }
    }
  ],
  "simpletype": "`array`"
}
```

### Output Example

```json
{ "parameters": [{ "parameter": "exercitation sunt", "in": ["proident laboris voluptate"] }] }
```

## list_operate

### List down supported usp operate commands

Commands will be shown in schema format

`list_operate`

- type: `Method`

### list_operate Type

`object` with following properties:

| Property | Type   | Required |
| -------- | ------ | -------- |
| `input`  | object | Optional |
| `output` | object | Optional |

#### input

`input`

- is optional
- type: `object`

##### input Type

`object` with following properties:

| Property | Type | Required |
| -------- | ---- | -------- |
| None     | None | None     |

### Ubus CLI Example

```
ubus call usp.raw list_operate {}
```

### JSONRPC Example

```json
{ "jsonrpc": "2.0", "id": 0, "method": "call", "params": ["<SID>", "usp.raw", "list_operate", {}] }
```

#### output

`output`

- is optional
- type: `object`

##### output Type

`object` with following properties:

| Property     | Type  | Required     |
| ------------ | ----- | ------------ |
| `parameters` | array | **Required** |

#### parameters

`parameters`

- is **required**
- type: `array`

##### parameters Type

Array type: `array`

All items must be of the type: Unknown type ``.

```json
{
  "type": "array",
  "items": [
    {
      "type": "object",
      "required": ["parameter", "type"],
      "properties": {
        "parameter": {
          "$ref": "#/definitions/operate_path_t"
        },
        "type": {
          "$ref": "#/definitions/operate_type_t"
        },
        "in": {
          "type": "array",
          "items": [
            {
              "type": "string"
            }
          ]
        },
        "out": {
          "type": "array",
          "items": [
            {
              "type": "string"
            }
          ]
        }
      }
    }
  ],
  "simpletype": "`array`"
}
```

### Output Example

```json
{
  "parameters": [
    {
      "parameter": "labore dolor",
      "type": "async",
      "in": ["sit tempor reprehenderit et"],
      "out": ["sit cupidatat adipisicing velit"]
    }
  ]
}
```

## object_names

### Get objects names

Get names of all the objects below input object path

`object_names`

- type: `Method`

### object_names Type

`object` with following properties:

| Property | Type   | Required     |
| -------- | ------ | ------------ |
| `input`  | object | **Required** |
| `output` | object | Optional     |

#### input

`input`

- is **required**
- type: `object`

##### input Type

`object` with following properties:

| Property | Type   | Required     | Default  |
| -------- | ------ | ------------ | -------- |
| `path`   | string | **Required** |          |
| `proto`  | string | Optional     | `"both"` |

#### path

DM object path with search queries

`path`

- is **required**
- type: reference

##### path Type

`string`

- minimum length: 6 characters
- maximum length: 256 characters

##### path Examples

```json
Device.
```

```json
Device.DeviceInfo.Manufacturer
```

```json
Device.WiFi.SSID.[SSID=="test_ssid"].BSSID
```

```json
Device.WiFi.SSID.*.BSSID
```

```json
Device.WiFi.SSID.[SSID!="test_ssid"&&Enable==1].BSSID
```

```json
Device.WiFi.
```

#### proto

`proto`

- is optional
- type: reference
- default: `"both"`

##### proto Type

`string`

The value of this property **must** be equal to one of the [known values below](#object_names-known-values).

##### proto Known Values

| Value |
| ----- |
| usp   |
| cwmp  |
| both  |

### Ubus CLI Example

```
ubus call usp.raw object_names {"path":"deserunt","proto":"both"}
```

### JSONRPC Example

```json
{
  "jsonrpc": "2.0",
  "id": 0,
  "method": "call",
  "params": ["<SID>", "usp.raw", "object_names", { "path": "deserunt", "proto": "both" }]
}
```

#### output

`output`

- is optional
- type: `object`

##### output Type

`object` with following properties:

| Property     | Type  | Required     |
| ------------ | ----- | ------------ |
| `parameters` | array | **Required** |

#### parameters

`parameters`

- is **required**
- type: `array`

##### parameters Type

Array type: `array`

All items must be of the type: Unknown type ``.

```json
{
  "type": "array",
  "items": [
    {
      "type": "object",
      "properties": {
        "parameter": {
          "$ref": "#/definitions/path_t"
        }
      }
    }
  ],
  "simpletype": "`array`"
}
```

### Output Example

```json
{ "parameters": [{ "parameter": "consectetur" }] }
```

## operate

### Operate handler

Operate on object element provided in path

`operate`

- type: `Method`

### operate Type

`object` with following properties:

| Property | Type   | Required     |
| -------- | ------ | ------------ |
| `input`  | object | **Required** |
| `output` | object | Optional     |

#### input

`input`

- is **required**
- type: `object`

##### input Type

`object` with following properties:

| Property | Type   | Required     | Default  |
| -------- | ------ | ------------ | -------- |
| `action` | string | **Required** |          |
| `input`  | object | Optional     |          |
| `path`   | string | **Required** |          |
| `proto`  | string | Optional     | `"both"` |

#### action

Opreate command as defined in TR-369, TR-181-2.13

`action`

- is **required**
- type: `string`

##### action Type

`string`

##### action Example

```json
{ "path": "Device.WiFi.", "action": "Reset()" }
```

#### input

Input arguments for the operate command as defined in TR-181-2.13

`input`

- is optional
- type: `object`

##### input Type

`object` with following properties:

| Property | Type | Required |
| -------- | ---- | -------- |
| None     | None | None     |

##### input Example

```json
{ "path": "Device.IP.Diagnostics", "action": "IPPing()", "input": { "Host": "iopsys.eu" } }
```

#### path

DM object path with search queries

`path`

- is **required**
- type: reference

##### path Type

`string`

- minimum length: 6 characters
- maximum length: 256 characters

##### path Examples

```json
Device.
```

```json
Device.DeviceInfo.Manufacturer
```

```json
Device.WiFi.SSID.[SSID=="test_ssid"].BSSID
```

```json
Device.WiFi.SSID.*.BSSID
```

```json
Device.WiFi.SSID.[SSID!="test_ssid"&&Enable==1].BSSID
```

```json
Device.WiFi.
```

#### proto

`proto`

- is optional
- type: reference
- default: `"both"`

##### proto Type

`string`

The value of this property **must** be equal to one of the [known values below](#operate-known-values).

##### proto Known Values

| Value |
| ----- |
| usp   |
| cwmp  |
| both  |

### Ubus CLI Example

```
ubus call usp.raw operate {"path":"fugiat dolore","action":"sint in adipisicing","proto":"cwmp","input":{}}
```

### JSONRPC Example

```json
{
  "jsonrpc": "2.0",
  "id": 0,
  "method": "call",
  "params": [
    "<SID>",
    "usp.raw",
    "operate",
    { "path": "fugiat dolore", "action": "sint in adipisicing", "proto": "cwmp", "input": {} }
  ]
}
```

#### output

Output will have status for sync commands and for async commands parameters as defined in TR-181-2.13

`output`

- is optional
- type: `object`

##### output Type

`object` with following properties:

| Property | Type | Required |
| -------- | ---- | -------- |
| None     | None | None     |

##### output Examples

```json
{
  "status": true
}
```

```json
{
  "AverageResponseTime": "0",
  "AverageResponseTimeDetailed": "130",
  "FailureCount": "0",
  "MaximumResponseTime": "0",
  "MaximumResponseTimeDetailed": "140",
  "MinimumResponseTime": "0",
  "MinimumResponseTimeDetailed": "120",
  "SuccessCount": "3"
}
```

### Output Example

```json
{}
```

## set

### Set handler

Set values of datamodel object element

`set`

- type: `Method`

### set Type

`object` with following properties:

| Property | Type   | Required     |
| -------- | ------ | ------------ |
| `input`  | object | **Required** |
| `output` |        | Optional     |

#### input

`input`

- is **required**
- type: `object`

##### input Type

`object` with following properties:

| Property         | Type    | Required     | Default  |
| ---------------- | ------- | ------------ | -------- |
| `instance_mode`  | integer | Optional     |          |
| `key`            | string  | Optional     |          |
| `path`           | string  | **Required** |          |
| `proto`          | string  | Optional     | `"both"` |
| `transaction_id` | integer | **Required** |          |
| `value`          | string  | **Required** |          |
| `values`         | object  | Optional     |          |

#### instance_mode

`instance_mode`

- is optional
- type: `integer`

##### instance_mode Type

`integer`

- minimum value: `0`
- maximum value: `1`

#### key

`key`

- is optional
- type: `string`

##### key Type

`string`

#### path

DM object path with search queries

`path`

- is **required**
- type: reference

##### path Type

`string`

- minimum length: 6 characters
- maximum length: 256 characters

##### path Examples

```json
Device.
```

```json
Device.DeviceInfo.Manufacturer
```

```json
Device.WiFi.SSID.[SSID=="test_ssid"].BSSID
```

```json
Device.WiFi.SSID.*.BSSID
```

```json
Device.WiFi.SSID.[SSID!="test_ssid"&&Enable==1].BSSID
```

```json
Device.WiFi.
```

#### proto

`proto`

- is optional
- type: reference
- default: `"both"`

##### proto Type

`string`

The value of this property **must** be equal to one of the [known values below](#set-known-values).

##### proto Known Values

| Value |
| ----- |
| usp   |
| cwmp  |
| both  |

#### transaction_id

`transaction_id`

- is **required**
- type: `integer`

##### transaction_id Type

`integer`

- minimum value: `1`

#### value

value of the object element provided in path, path should contains valid writable object element

`value`

- is **required**
- type: `string`

##### value Type

`string`

##### value Examples

```json
{ "path": "Device.WiFi.SSID.1.SSID", "value": "test_ssid" }
```

```json
{ "path": "Device.WiFi.SSID.2.Enable", "value": "true" }
```

```json
{ "path": "Device.WiFi.SSID.1.Enable", "value": "0" }
```

#### values

To set multiple values at once, path should be relative to object elements

`values`

- is optional
- type: `object`

##### values Type

`object` with following properties:

| Property | Type | Required |
| -------- | ---- | -------- |
| None     | None | None     |

##### values Examples

```json
{ "path": "Device.WiFi.SSID.1", "values": { ".SSID": "test_ssid", ".Name": "test_name" } }
```

```json
{ "path": "Device.WiFi.SSID.2", "values": { ".SSID": "test_ssid" } }
```

### Ubus CLI Example

```
ubus call usp.raw set {"path":"aliqui","value":"","transaction_id":23221830,"proto":"usp","values":{},"key":"in consectetur sit magna","instance_mode":0}
```

### JSONRPC Example

```json
{
  "jsonrpc": "2.0",
  "id": 0,
  "method": "call",
  "params": [
    "<SID>",
    "usp.raw",
    "set",
    {
      "path": "aliqui",
      "value": "",
      "transaction_id": 23221830,
      "proto": "usp",
      "values": {},
      "key": "in consectetur sit magna",
      "instance_mode": 0
    }
  ]
}
```

#### output

`output`

- is optional
- type: complex

##### output Type

Unknown type ``.

```json
{
  "oneof": [
    {
      "type": "object",
      "properties": {
        "status": {
          "const": "1"
        }
      }
    },
    {
      "type": "object",
      "required": ["parameters"],
      "properties": {
        "parameters": {
          "type": "array",
          "items": [
            {
              "type": "object",
              "properties": {
                "path": {
                  "$ref": "#/definitions/path_t"
                },
                "status": {
                  "const": "0"
                },
                "fault": {
                  "$ref": "#/definitions/fault_t"
                }
              }
            }
          ]
        }
      }
    }
  ],
  "definitions": {
    "path_t": {
      "description": "Complete object element path as per TR181",
      "type": "string",
      "minLength": 6,
      "maxLength": 256,
      "examples": ["Device.", "Device.DeviceInfo.Manufacturer", "Device.WiFi.SSID.1.", "Device.WiFi."]
    },
    "schema_path_t": {
      "description": "Datamodel object schema path",
      "type": "string",
      "minLength": 6,
      "maxLength": 256,
      "examples": ["Device.Bridging.Bridge.{i}.", "Device.DeviceInfo.Manufacturer", "Device.WiFi.SSID.{i}.SSID"]
    },
    "boolean_t": {
      "type": "string",
      "enum": ["0", "1"]
    },
    "operate_path_t": {
      "description": "Datamodel object schema path",
      "type": "string",
      "minLength": 6,
      "maxLength": 256,
      "examples": ["Device.DHCPv4.Client.{i}.Renew()", "Device.FactoryReset()"]
    },
    "operate_type_t": {
      "type": "string",
      "enum": ["async", "sync"]
    },
    "query_path_t": {
      "description": "DM object path with search queries",
      "type": "string",
      "minLength": 6,
      "maxLength": 256,
      "examples": [
        "Device.",
        "Device.DeviceInfo.Manufacturer",
        "Device.WiFi.SSID.[SSID==\"test_ssid\"].BSSID",
        "Device.WiFi.SSID.*.BSSID",
        "Device.WiFi.SSID.[SSID!=\"test_ssid\"&&Enable==1].BSSID",
        "Device.WiFi."
      ]
    },
    "instance_t": {
      "description": "Multi object instances",
      "type": "string",
      "minLength": 6,
      "maxLength": 256
    },
    "proto_t": {
      "type": "string",
      "default": "both",
      "enum": ["usp", "cwmp", "both"]
    },
    "type_t": {
      "type": "string",
      "enum": [
        "xsd:string",
        "xsd:unsignedInt",
        "xsd:int",
        "xsd:unsignedLong",
        "xsd:long",
        "xsd:boolean",
        "xsd:dateTime",
        "xsd:hexBinary",
        "xsd:object"
      ]
    },
    "fault_t": {
      "type": "integer",
      "minimum": 7000,
      "maximum": 9050
    }
  },
  "out": "{\"oneof\":[{\"status\":\"1\"},{\"parameters\":[{\"path\":\"labore et sit dolor sunt\",\"status\":\"0\",\"fault\":7485}]}],\"definitions\":{\"path_t\":{\"description\":\"Complete object element path as per TR181\",\"type\":\"string\",\"minLength\":6,\"maxLength\":256,\"examples\":[\"Device.\",\"Device.DeviceInfo.Manufacturer\",\"Device.WiFi.SSID.1.\",\"Device.WiFi.\"]},\"schema_path_t\":{\"description\":\"Datamodel object schema path\",\"type\":\"string\",\"minLength\":6,\"maxLength\":256,\"examples\":[\"Device.Bridging.Bridge.{i}.\",\"Device.DeviceInfo.Manufacturer\",\"Device.WiFi.SSID.{i}.SSID\"]},\"boolean_t\":{\"type\":\"string\",\"enum\":[\"0\",\"1\"]},\"operate_path_t\":{\"description\":\"Datamodel object schema path\",\"type\":\"string\",\"minLength\":6,\"maxLength\":256,\"examples\":[\"Device.DHCPv4.Client.{i}.Renew()\",\"Device.FactoryReset()\"]},\"operate_type_t\":{\"type\":\"string\",\"enum\":[\"async\",\"sync\"]},\"query_path_t\":{\"description\":\"DM object path with search queries\",\"type\":\"string\",\"minLength\":6,\"maxLength\":256,\"examples\":[\"Device.\",\"Device.DeviceInfo.Manufacturer\",\"Device.WiFi.SSID.[SSID==\\\"test_ssid\\\"].BSSID\",\"Device.WiFi.SSID.*.BSSID\",\"Device.WiFi.SSID.[SSID!=\\\"test_ssid\\\"&&Enable==1].BSSID\",\"Device.WiFi.\"]},\"instance_t\":{\"description\":\"Multi object instances\",\"type\":\"string\",\"minLength\":6,\"maxLength\":256},\"proto_t\":{\"type\":\"string\",\"default\":\"both\",\"enum\":[\"usp\",\"cwmp\",\"both\"]},\"type_t\":{\"type\":\"string\",\"enum\":[\"xsd:string\",\"xsd:unsignedInt\",\"xsd:int\",\"xsd:unsignedLong\",\"xsd:long\",\"xsd:boolean\",\"xsd:dateTime\",\"xsd:hexBinary\",\"xsd:object\"]},\"fault_t\":{\"type\":\"integer\",\"minimum\":7000,\"maximum\":9050}}}",
  "simpletype": "complex"
}
```

### Output Example

```json
{
  "oneof": [
    { "status": "1" },
    { "parameters": [{ "path": "labore et sit dolor sunt", "status": "0", "fault": 7485 }] }
  ],
  "definitions": {
    "path_t": {
      "description": "Complete object element path as per TR181",
      "type": "string",
      "minLength": 6,
      "maxLength": 256,
      "examples": ["Device.", "Device.DeviceInfo.Manufacturer", "Device.WiFi.SSID.1.", "Device.WiFi."]
    },
    "schema_path_t": {
      "description": "Datamodel object schema path",
      "type": "string",
      "minLength": 6,
      "maxLength": 256,
      "examples": ["Device.Bridging.Bridge.{i}.", "Device.DeviceInfo.Manufacturer", "Device.WiFi.SSID.{i}.SSID"]
    },
    "boolean_t": { "type": "string", "enum": ["0", "1"] },
    "operate_path_t": {
      "description": "Datamodel object schema path",
      "type": "string",
      "minLength": 6,
      "maxLength": 256,
      "examples": ["Device.DHCPv4.Client.{i}.Renew()", "Device.FactoryReset()"]
    },
    "operate_type_t": { "type": "string", "enum": ["async", "sync"] },
    "query_path_t": {
      "description": "DM object path with search queries",
      "type": "string",
      "minLength": 6,
      "maxLength": 256,
      "examples": [
        "Device.",
        "Device.DeviceInfo.Manufacturer",
        "Device.WiFi.SSID.[SSID==\"test_ssid\"].BSSID",
        "Device.WiFi.SSID.*.BSSID",
        "Device.WiFi.SSID.[SSID!=\"test_ssid\"&&Enable==1].BSSID",
        "Device.WiFi."
      ]
    },
    "instance_t": { "description": "Multi object instances", "type": "string", "minLength": 6, "maxLength": 256 },
    "proto_t": { "type": "string", "default": "both", "enum": ["usp", "cwmp", "both"] },
    "type_t": {
      "type": "string",
      "enum": [
        "xsd:string",
        "xsd:unsignedInt",
        "xsd:int",
        "xsd:unsignedLong",
        "xsd:long",
        "xsd:boolean",
        "xsd:dateTime",
        "xsd:hexBinary",
        "xsd:object"
      ]
    },
    "fault_t": { "type": "integer", "minimum": 7000, "maximum": 9050 }
  }
}
```

## setm_values

### Set values of multiple objects at once

`setm_values`

- type: `Method`

### setm_values Type

`object` with following properties:

| Property | Type   | Required     |
| -------- | ------ | ------------ |
| `input`  | object | **Required** |
| `output` | object | **Required** |

#### input

`input`

- is **required**
- type: `object`

##### input Type

`object` with following properties:

| Property         | Type    | Required     | Default  |
| ---------------- | ------- | ------------ | -------- |
| `instance_mode`  | integer | Optional     |          |
| `key`            | string  | Optional     |          |
| `proto`          | string  | Optional     | `"both"` |
| `pv_tuple`       | array   | **Required** |          |
| `transaction_id` | integer | **Required** |          |

#### instance_mode

`instance_mode`

- is optional
- type: `integer`

##### instance_mode Type

`integer`

- minimum value: `0`
- maximum value: `1`

#### key

`key`

- is optional
- type: `string`

##### key Type

`string`

#### proto

`proto`

- is optional
- type: reference
- default: `"both"`

##### proto Type

`string`

The value of this property **must** be equal to one of the [known values below](#setm_values-known-values).

##### proto Known Values

| Value |
| ----- |
| usp   |
| cwmp  |
| both  |

#### pv_tuple

`pv_tuple`

- is **required**
- type: `array`

##### pv_tuple Type

Array type: `array`

All items must be of the type: Unknown type ``.

```json
{
  "type": "array",
  "items": [
    {
      "type": "object",
      "required": ["path", "value"],
      "properties": {
        "path": {
          "$ref": "#/definitions/path_t"
        },
        "value": {
          "type": "string"
        },
        "key": {
          "type": "string"
        }
      }
    }
  ],
  "simpletype": "`array`"
}
```

#### transaction_id

`transaction_id`

- is **required**
- type: `integer`

##### transaction_id Type

`integer`

- minimum value: `0`

### Ubus CLI Example

```
ubus call usp.raw setm_values {"pv_tuple":[{"path":"pariatur in ut","value":"nostrud aute elit ipsum","key":"laboris cupidatat adipisicing qui"}],"transaction_id":86006430,"proto":"cwmp","key":"consequat Excepteur officia et","instance_mode":0}
```

### JSONRPC Example

```json
{
  "jsonrpc": "2.0",
  "id": 0,
  "method": "call",
  "params": [
    "<SID>",
    "usp.raw",
    "setm_values",
    {
      "pv_tuple": [
        { "path": "pariatur in ut", "value": "nostrud aute elit ipsum", "key": "laboris cupidatat adipisicing qui" }
      ],
      "transaction_id": 86006430,
      "proto": "cwmp",
      "key": "consequat Excepteur officia et",
      "instance_mode": 0
    }
  ]
}
```

#### output

`output`

- is **required**
- type: `object`

##### output Type

`object` with following properties:

| Property     | Type    | Required |
| ------------ | ------- | -------- |
| `parameters` | array   | Optional |
| `status`     | boolean | Optional |

#### parameters

`parameters`

- is optional
- type: `array`

##### parameters Type

Array type: `array`

All items must be of the type: Unknown type ``.

```json
{
  "type": "array",
  "items": [
    {
      "type": "object",
      "properties": {
        "path": {
          "$ref": "#/definitions/path_t"
        },
        "status": {
          "type": "boolean"
        },
        "fault": {
          "$ref": "#/definitions/fault_t"
        }
      }
    }
  ],
  "simpletype": "`array`"
}
```

#### status

`status`

- is optional
- type: `boolean`

##### status Type

`boolean`

### Output Example

```json
{ "status": true, "parameters": [{ "path": "ut veniam", "status": true, "fault": 7227 }] }
```

## transaction_abort

### Aborts a on-going transaction

`transaction_abort`

- type: `Method`

### transaction_abort Type

`object` with following properties:

| Property | Type   | Required     |
| -------- | ------ | ------------ |
| `input`  | object | **Required** |
| `output` | object | **Required** |

#### input

`input`

- is **required**
- type: `object`

##### input Type

`object` with following properties:

| Property         | Type    | Required     |
| ---------------- | ------- | ------------ |
| `transaction_id` | integer | **Required** |

#### transaction_id

`transaction_id`

- is **required**
- type: `integer`

##### transaction_id Type

`integer`

- minimum value: `1`

### Ubus CLI Example

```
ubus call usp.raw transaction_abort {"transaction_id":57220780}
```

### JSONRPC Example

```json
{
  "jsonrpc": "2.0",
  "id": 0,
  "method": "call",
  "params": ["<SID>", "usp.raw", "transaction_abort", { "transaction_id": 57220780 }]
}
```

#### output

`output`

- is **required**
- type: `object`

##### output Type

`object` with following properties:

| Property | Type    | Required     |
| -------- | ------- | ------------ |
| `error`  | string  | Optional     |
| `status` | boolean | **Required** |

#### error

`error`

- is optional
- type: `string`

##### error Type

`string`

#### status

`status`

- is **required**
- type: `boolean`

##### status Type

`boolean`

### Output Example

```json
{ "status": false, "error": "qui incididunt amet" }
```

## transaction_commit

### Commits an on-going transaction

`transaction_commit`

- type: `Method`

### transaction_commit Type

`object` with following properties:

| Property | Type   | Required     |
| -------- | ------ | ------------ |
| `input`  | object | **Required** |
| `output` | object | **Required** |

#### input

`input`

- is **required**
- type: `object`

##### input Type

`object` with following properties:

| Property           | Type    | Required     |
| ------------------ | ------- | ------------ |
| `restart_services` | boolean | Optional     |
| `transaction_id`   | integer | **Required** |

#### restart_services

`restart_services`

- is optional
- type: `boolean`

##### restart_services Type

`boolean`

#### transaction_id

`transaction_id`

- is **required**
- type: `integer`

##### transaction_id Type

`integer`

- minimum value: `1`

### Ubus CLI Example

```
ubus call usp.raw transaction_commit {"transaction_id":9024232,"restart_services":false}
```

### JSONRPC Example

```json
{
  "jsonrpc": "2.0",
  "id": 0,
  "method": "call",
  "params": ["<SID>", "usp.raw", "transaction_commit", { "transaction_id": 9024232, "restart_services": false }]
}
```

#### output

`output`

- is **required**
- type: `object`

##### output Type

`object` with following properties:

| Property           | Type    | Required     |
| ------------------ | ------- | ------------ |
| `error`            | string  | Optional     |
| `status`           | boolean | **Required** |
| `updated_services` | array   | Optional     |

#### error

`error`

- is optional
- type: `string`

##### error Type

`string`

#### status

`status`

- is **required**
- type: `boolean`

##### status Type

`boolean`

#### updated_services

`updated_services`

- is optional
- type: `array`

##### updated_services Type

Array type: `array`

All items must be of the type: Unknown type ``.

```json
{
  "type": "array",
  "items": [
    {
      "type": "string"
    }
  ],
  "simpletype": "`array`"
}
```

### Output Example

```json
{ "status": true, "error": "sit fug", "updated_services": ["pariatur elit in"] }
```

## transaction_start

### Start a transaction before set/add/del operations

`transaction_start`

- type: `Method`

### transaction_start Type

`object` with following properties:

| Property | Type   | Required     |
| -------- | ------ | ------------ |
| `input`  | object | **Required** |
| `output` | object | **Required** |

#### input

`input`

- is **required**
- type: `object`

##### input Type

`object` with following properties:

| Property | Type   | Required     |
| -------- | ------ | ------------ |
| `app`    | string | **Required** |

#### app

`app`

- is **required**
- type: `string`

##### app Type

`string`

### Ubus CLI Example

```
ubus call usp.raw transaction_start {"app":"sed adipisicing"}
```

### JSONRPC Example

```json
{
  "jsonrpc": "2.0",
  "id": 0,
  "method": "call",
  "params": ["<SID>", "usp.raw", "transaction_start", { "app": "sed adipisicing" }]
}
```

#### output

`output`

- is **required**
- type: `object`

##### output Type

`object` with following properties:

| Property         | Type    | Required     |
| ---------------- | ------- | ------------ |
| `error`          | string  | Optional     |
| `status`         | boolean | **Required** |
| `transaction_id` | integer | Optional     |

#### error

`error`

- is optional
- type: `string`

##### error Type

`string`

#### status

`status`

- is **required**
- type: `boolean`

##### status Type

`boolean`

#### transaction_id

`transaction_id`

- is optional
- type: `integer`

##### transaction_id Type

`integer`

- minimum value: `1`

### Output Example

```json
{ "status": false, "transaction_id": 24672867, "error": "labore incididunt dolor" }
```

## validate

### Validate a datamodel path

Validate a path

`validate`

- type: `Method`

### validate Type

`object` with following properties:

| Property | Type   | Required     |
| -------- | ------ | ------------ |
| `input`  | object | **Required** |
| `output` | object | Optional     |

#### input

`input`

- is **required**
- type: `object`

##### input Type

`object` with following properties:

| Property | Type   | Required     | Default  |
| -------- | ------ | ------------ | -------- |
| `path`   | string | **Required** |          |
| `proto`  | string | Optional     | `"both"` |

#### path

DM object path with search queries

`path`

- is **required**
- type: reference

##### path Type

`string`

- minimum length: 6 characters
- maximum length: 256 characters

##### path Examples

```json
Device.
```

```json
Device.DeviceInfo.Manufacturer
```

```json
Device.WiFi.SSID.[SSID=="test_ssid"].BSSID
```

```json
Device.WiFi.SSID.*.BSSID
```

```json
Device.WiFi.SSID.[SSID!="test_ssid"&&Enable==1].BSSID
```

```json
Device.WiFi.
```

#### proto

`proto`

- is optional
- type: reference
- default: `"both"`

##### proto Type

`string`

The value of this property **must** be equal to one of the [known values below](#validate-known-values).

##### proto Known Values

| Value |
| ----- |
| usp   |
| cwmp  |
| both  |

### Ubus CLI Example

```
ubus call usp.raw validate {"path":"reprehenderit","proto":"both"}
```

### JSONRPC Example

```json
{
  "jsonrpc": "2.0",
  "id": 0,
  "method": "call",
  "params": ["<SID>", "usp.raw", "validate", { "path": "reprehenderit", "proto": "both" }]
}
```

#### output

`output`

- is optional
- type: `object`

##### output Type

`object` with following properties:

| Property     | Type  | Required     |
| ------------ | ----- | ------------ |
| `parameters` | array | **Required** |

#### parameters

`parameters`

- is **required**
- type: `array`

##### parameters Type

Array type: `array`

All items must be of the type: Unknown type ``.

```json
{
  "type": "array",
  "items": [
    {
      "type": "object",
      "properties": {
        "parameter": {
          "$ref": "#/definitions/path_t"
        },
        "fault": {
          "$ref": "#/definitions/fault_t"
        }
      }
    }
  ],
  "simpletype": "`array`"
}
```

### Output Example

```json
{ "parameters": [{ "parameter": "inreprehenderit", "fault": 8454 }] }
```
