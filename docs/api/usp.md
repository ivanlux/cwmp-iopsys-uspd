# usp Schema

```
https://www.iopsys.eu/usp.raw.json
```

| Custom Properties | Additional Properties |
| ----------------- | --------------------- |
| Forbidden         | Forbidden             |

# usp

| List of Methods               |
| ----------------------------- |
| [add_object](#add_object)     | Method | usp (this schema) |
| [del_object](#del_object)     | Method | usp (this schema) |
| [get](#get)                   | Method | usp (this schema) |
| [instances](#instances)       | Method | usp (this schema) |
| [list_operate](#list_operate) | Method | usp (this schema) |
| [object_names](#object_names) | Method | usp (this schema) |
| [operate](#operate)           | Method | usp (this schema) |
| [set](#set)                   | Method | usp (this schema) |
| [validate](#validate)         | Method | usp (this schema) |

## add_object

### Add a new object instance

Add a new object in multi instance object

`add_object`

- type: `Method`

### add_object Type

`object` with following properties:

| Property | Type   | Required     |
| -------- | ------ | ------------ |
| `input`  | object | **Required** |
| `output` | object | **Required** |

#### input

`input`

- is **required**
- type: `object`

##### input Type

`object` with following properties:

| Property | Type   | Required     | Default  |
| -------- | ------ | ------------ | -------- |
| `path`   | string | **Required** |          |
| `proto`  | string | Optional     | `"both"` |

#### path

Complete object element path as per TR181

`path`

- is **required**
- type: reference

##### path Type

`string`

- minimum length: 6 characters
- maximum length: 256 characters

##### path Examples

```json
Device.
```

```json
Device.DeviceInfo.Manufacturer
```

```json
Device.WiFi.SSID.1.
```

```json
Device.WiFi.
```

#### proto

`proto`

- is optional
- type: reference
- default: `"both"`

##### proto Type

`string`

The value of this property **must** be equal to one of the [known values below](#add_object-known-values).

##### proto Known Values

| Value |
| ----- |
| usp   |
| cwmp  |
| both  |

### Ubus CLI Example

```
ubus call usp add_object {"path":"ea Excepteur Lorem in in","proto":"usp"}
```

### JSONRPC Example

```json
{
  "jsonrpc": "2.0",
  "id": 0,
  "method": "call",
  "params": ["<SID>", "usp", "add_object", { "path": "ea Excepteur Lorem in in", "proto": "usp" }]
}
```

#### output

`output`

- is **required**
- type: `object`

##### output Type

`object` with following properties:

| Property   | Type    | Required |
| ---------- | ------- | -------- |
| `fault`    | integer | Optional |
| `instance` | integer | Optional |
| `status`   | boolean | Optional |

#### fault

`fault`

- is optional
- type: reference

##### fault Type

`integer`

- minimum value: `7000`
- maximum value: `9050`

#### instance

`instance`

- is optional
- type: `integer`

##### instance Type

`integer`

- minimum value: `1`

#### status

`status`

- is optional
- type: `boolean`

##### status Type

`boolean`

### Output Example

```json
{ "status": true, "instance": 63518717, "fault": 8921 }
```

## del_object

### Delete object instance

Delete a object instance from multi instance object

`del_object`

- type: `Method`

### del_object Type

`object` with following properties:

| Property | Type   | Required     |
| -------- | ------ | ------------ |
| `input`  | object | **Required** |
| `output` | object | **Required** |

#### input

`input`

- is **required**
- type: `object`

##### input Type

`object` with following properties:

| Property | Type   | Required     | Default  |
| -------- | ------ | ------------ | -------- |
| `path`   | string | **Required** |          |
| `proto`  | string | Optional     | `"both"` |

#### path

DM object path with search queries

`path`

- is **required**
- type: reference

##### path Type

`string`

- minimum length: 6 characters
- maximum length: 256 characters

##### path Examples

```json
Device.
```

```json
Device.DeviceInfo.Manufacturer
```

```json
Device.WiFi.SSID.[SSID=="test_ssid"].BSSID
```

```json
Device.WiFi.SSID.*.BSSID
```

```json
Device.WiFi.SSID.[SSID!="test_ssid"&&Enable==1].BSSID
```

```json
Device.WiFi.
```

#### proto

`proto`

- is optional
- type: reference
- default: `"both"`

##### proto Type

`string`

The value of this property **must** be equal to one of the [known values below](#del_object-known-values).

##### proto Known Values

| Value |
| ----- |
| usp   |
| cwmp  |
| both  |

### Ubus CLI Example

```
ubus call usp del_object {"path":"magna adip","proto":"both"}
```

### JSONRPC Example

```json
{
  "jsonrpc": "2.0",
  "id": 0,
  "method": "call",
  "params": ["<SID>", "usp", "del_object", { "path": "magna adip", "proto": "both" }]
}
```

#### output

`output`

- is **required**
- type: `object`

##### output Type

`object` with following properties:

| Property | Type    | Required |
| -------- | ------- | -------- |
| `fault`  | integer | Optional |
| `status` | boolean | Optional |

#### fault

`fault`

- is optional
- type: reference

##### fault Type

`integer`

- minimum value: `7000`
- maximum value: `9050`

#### status

`status`

- is optional
- type: `boolean`

##### status Type

`boolean`

### Output Example

```json
{ "status": false, "fault": 8468 }
```

## get

### Get handler

Query the datamodel object

`get`

- type: `Method`

### get Type

`object` with following properties:

| Property | Type   | Required     |
| -------- | ------ | ------------ |
| `input`  | object | **Required** |
| `output` | object | **Required** |

#### input

`input`

- is **required**
- type: `object`

##### input Type

`object` with following properties:

| Property | Type   | Required     | Default  |
| -------- | ------ | ------------ | -------- |
| `path`   | string | **Required** |          |
| `proto`  | string | Optional     | `"both"` |

#### path

DM object path with search queries

`path`

- is **required**
- type: reference

##### path Type

`string`

- minimum length: 6 characters
- maximum length: 256 characters

##### path Examples

```json
Device.
```

```json
Device.DeviceInfo.Manufacturer
```

```json
Device.WiFi.SSID.[SSID=="test_ssid"].BSSID
```

```json
Device.WiFi.SSID.*.BSSID
```

```json
Device.WiFi.SSID.[SSID!="test_ssid"&&Enable==1].BSSID
```

```json
Device.WiFi.
```

#### proto

`proto`

- is optional
- type: reference
- default: `"both"`

##### proto Type

`string`

The value of this property **must** be equal to one of the [known values below](#get-known-values).

##### proto Known Values

| Value |
| ----- |
| usp   |
| cwmp  |
| both  |

### Ubus CLI Example

```
ubus call usp get {"path":"aute Duis esse nostrud sunt","proto":"both"}
```

### JSONRPC Example

```json
{
  "jsonrpc": "2.0",
  "id": 0,
  "method": "call",
  "params": ["<SID>", "usp", "get", { "path": "aute Duis esse nostrud sunt", "proto": "both" }]
}
```

#### output

`output`

- is **required**
- type: `object`

##### output Type

`object` with following properties:

| Property | Type | Required |
| -------- | ---- | -------- |
| None     | None | None     |

##### output Examples

```json
root@iopsys:/tmp# ubus call usp get '{"path":"Device.Users.User.2."}'
{
	"User": [
		{
			"Alias": "",
			"Enable": true,
			"Language": "",
			"Password": "",
			"RemoteAccessCapable": false,
			"Username": "user_2"
		}
	]
}
```

```json
root@iopsys:/tmp# ubus call usp get '{"path":"Device.Users."}'
{
	"Users": {
		"User": [
			{
				"Alias": "",
				"Enable": true,
				"Language": "",
				"Password": "",
				"RemoteAccessCapable": true,
				"Username": "user"
			},
			{
				"Alias": "",
				"Enable": true,
				"Language": "",
				"Password": "",
				"RemoteAccessCapable": false,
				"Username": "user_2"
			}
		],
		"UserNumberOfEntries": 2
	}
}
```

### Output Example

```json
{}
```

## instances

### Instance query handler

Get the instances of multi object

`instances`

- type: `Method`

### instances Type

`object` with following properties:

| Property | Type   | Required     |
| -------- | ------ | ------------ |
| `input`  | object | **Required** |
| `output` | object | Optional     |

#### input

`input`

- is **required**
- type: `object`

##### input Type

`object` with following properties:

| Property | Type   | Required     | Default  |
| -------- | ------ | ------------ | -------- |
| `path`   | string | **Required** |          |
| `proto`  | string | Optional     | `"both"` |

#### path

DM object path with search queries

`path`

- is **required**
- type: reference

##### path Type

`string`

- minimum length: 6 characters
- maximum length: 256 characters

##### path Examples

```json
Device.
```

```json
Device.DeviceInfo.Manufacturer
```

```json
Device.WiFi.SSID.[SSID=="test_ssid"].BSSID
```

```json
Device.WiFi.SSID.*.BSSID
```

```json
Device.WiFi.SSID.[SSID!="test_ssid"&&Enable==1].BSSID
```

```json
Device.WiFi.
```

#### proto

`proto`

- is optional
- type: reference
- default: `"both"`

##### proto Type

`string`

The value of this property **must** be equal to one of the [known values below](#instances-known-values).

##### proto Known Values

| Value |
| ----- |
| usp   |
| cwmp  |
| both  |

### Ubus CLI Example

```
ubus call usp instances {"path":"velitut ad tempor dolore ut","proto":"usp"}
```

### JSONRPC Example

```json
{
  "jsonrpc": "2.0",
  "id": 0,
  "method": "call",
  "params": ["<SID>", "usp", "instances", { "path": "velitut ad tempor dolore ut", "proto": "usp" }]
}
```

#### output

`output`

- is optional
- type: `object`

##### output Type

`object` with following properties:

| Property     | Type  | Required     |
| ------------ | ----- | ------------ |
| `parameters` | array | **Required** |

#### parameters

`parameters`

- is **required**
- type: `object[]`

##### parameters Type

Array type: `object[]`

All items must be of the type: `object` with following properties:

| Property    | Type   | Required |
| ----------- | ------ | -------- |
| `parameter` | string | Optional |

#### parameter

Multi object instances

`parameter`

- is optional
- type: reference

##### parameter Type

`string`

- minimum length: 6 characters
- maximum length: 256 characters

### Output Example

```json
{ "parameters": [{ "parameter": "esteu sed Lorem enim" }, { "parameter": "consectetur voluptat" }] }
```

## list_operate

### List down supported usp operate commands

Commands will be shown in schema format

`list_operate`

- type: `Method`

### list_operate Type

`object` with following properties:

| Property | Type   | Required |
| -------- | ------ | -------- |
| `input`  | object | Optional |
| `output` | object | Optional |

#### input

`input`

- is optional
- type: `object`

##### input Type

`object` with following properties:

| Property | Type | Required |
| -------- | ---- | -------- |
| None     | None | None     |

### Ubus CLI Example

```
ubus call usp list_operate {}
```

### JSONRPC Example

```json
{ "jsonrpc": "2.0", "id": 0, "method": "call", "params": ["<SID>", "usp", "list_operate", {}] }
```

#### output

`output`

- is optional
- type: `object`

##### output Type

`object` with following properties:

| Property     | Type  | Required     |
| ------------ | ----- | ------------ |
| `parameters` | array | **Required** |

#### parameters

`parameters`

- is **required**
- type: `object[]`

##### parameters Type

Array type: `object[]`

All items must be of the type: `object` with following properties:

| Property    | Type   | Required     |
| ----------- | ------ | ------------ |
| `in`        | array  | Optional     |
| `out`       | array  | Optional     |
| `parameter` | string | **Required** |
| `type`      | string | **Required** |

#### in

`in`

- is optional
- type: `string[]`

##### in Type

Array type: `string[]`

All items must be of the type: `string`

#### out

`out`

- is optional
- type: `string[]`

##### out Type

Array type: `string[]`

All items must be of the type: `string`

#### parameter

Datamodel object schema path

`parameter`

- is **required**
- type: reference

##### parameter Type

`string`

- minimum length: 6 characters
- maximum length: 256 characters

##### parameter Examples

```json
Device.DHCPv4.Client.{i}.Renew()
```

```json
Device.FactoryReset()
```

#### type

`type`

- is **required**
- type: reference

##### type Type

`string`

The value of this property **must** be equal to one of the [known values below](#list_operate-known-values).

##### type Known Values

| Value |
| ----- |
| async |
| sync  |

### Output Example

```json
{
  "parameters": [
    {
      "parameter": "cupidatat Excepteur magna qui voluptate",
      "type": "async",
      "in": ["eu", "ipsum", "ipsum labore consequat in"],
      "out": ["id esse magna"]
    }
  ]
}
```

## object_names

### Get objects names

Get names of all the objects below input object path

`object_names`

- type: `Method`

### object_names Type

`object` with following properties:

| Property | Type   | Required     |
| -------- | ------ | ------------ |
| `input`  | object | **Required** |
| `output` | object | Optional     |

#### input

`input`

- is **required**
- type: `object`

##### input Type

`object` with following properties:

| Property | Type   | Required     | Default  |
| -------- | ------ | ------------ | -------- |
| `path`   | string | **Required** |          |
| `proto`  | string | Optional     | `"both"` |

#### path

DM object path with search queries

`path`

- is **required**
- type: reference

##### path Type

`string`

- minimum length: 6 characters
- maximum length: 256 characters

##### path Examples

```json
Device.
```

```json
Device.DeviceInfo.Manufacturer
```

```json
Device.WiFi.SSID.[SSID=="test_ssid"].BSSID
```

```json
Device.WiFi.SSID.*.BSSID
```

```json
Device.WiFi.SSID.[SSID!="test_ssid"&&Enable==1].BSSID
```

```json
Device.WiFi.
```

#### proto

`proto`

- is optional
- type: reference
- default: `"both"`

##### proto Type

`string`

The value of this property **must** be equal to one of the [known values below](#object_names-known-values).

##### proto Known Values

| Value |
| ----- |
| usp   |
| cwmp  |
| both  |

### Ubus CLI Example

```
ubus call usp object_names {"path":"consequat non","proto":"both"}
```

### JSONRPC Example

```json
{
  "jsonrpc": "2.0",
  "id": 0,
  "method": "call",
  "params": ["<SID>", "usp", "object_names", { "path": "consequat non", "proto": "both" }]
}
```

#### output

`output`

- is optional
- type: `object`

##### output Type

`object` with following properties:

| Property     | Type  | Required     |
| ------------ | ----- | ------------ |
| `parameters` | array | **Required** |

#### parameters

`parameters`

- is **required**
- type: `object[]`

##### parameters Type

Array type: `object[]`

All items must be of the type: `object` with following properties:

| Property    | Type   | Required |
| ----------- | ------ | -------- |
| `parameter` | string | Optional |

#### parameter

Complete object element path as per TR181

`parameter`

- is optional
- type: reference

##### parameter Type

`string`

- minimum length: 6 characters
- maximum length: 256 characters

##### parameter Examples

```json
Device.
```

```json
Device.DeviceInfo.Manufacturer
```

```json
Device.WiFi.SSID.1.
```

```json
Device.WiFi.
```

### Output Example

```json
{
  "parameters": [
    { "parameter": "ea proident irure vo" },
    { "parameter": "aute Excepteur" },
    { "parameter": "sintlaboris cupidatat deserunt eu" },
    { "parameter": "est Duis ut" }
  ]
}
```

## operate

### Operate handler

Operate on object element provided in path

`operate`

- type: `Method`

### operate Type

`object` with following properties:

| Property | Type   | Required     |
| -------- | ------ | ------------ |
| `input`  | object | **Required** |
| `output` | object | Optional     |

#### input

`input`

- is **required**
- type: `object`

##### input Type

`object` with following properties:

| Property | Type   | Required     | Default  |
| -------- | ------ | ------------ | -------- |
| `action` | string | **Required** |          |
| `input`  | object | Optional     |          |
| `path`   | string | **Required** |          |
| `proto`  | string | Optional     | `"both"` |

#### action

Opreate command as defined in TR-369, TR-181-2.13

`action`

- is **required**
- type: `string`

##### action Type

`string`

All instances must conform to this regular expression

```regex
[a-zA-Z]+\(\)
```

- test example:
  [{&amp;quot;path&amp;quot;:&amp;quot;Device.WiFi.&amp;quot;, &amp;quot;action&amp;quot;:&amp;quot;Reset\(\)&amp;quot;}](<https://regexr.com/?expression=%5Ba-zA-Z%5D%2B%5C(%5C)&text=%7B%22path%22%3A%22Device.WiFi.%22%2C%20%22action%22%3A%22Reset%5C(%5C)%22%7D>)

##### action Example

```json
{ "path": "Device.WiFi.", "action": "Reset()" }
```

#### input

Input arguments for the operate command as defined in TR-181-2.13

`input`

- is optional
- type: `object`

##### input Type

`object` with following properties:

| Property | Type | Required |
| -------- | ---- | -------- |
| None     | None | None     |

##### input Example

```json
{ "path": "Device.IP.Diagnostics", "action": "IPPing()", "input": { "Host": "iopsys.eu" } }
```

#### path

DM object path with search queries

`path`

- is **required**
- type: reference

##### path Type

`string`

- minimum length: 6 characters
- maximum length: 256 characters

##### path Examples

```json
Device.
```

```json
Device.DeviceInfo.Manufacturer
```

```json
Device.WiFi.SSID.[SSID=="test_ssid"].BSSID
```

```json
Device.WiFi.SSID.*.BSSID
```

```json
Device.WiFi.SSID.[SSID!="test_ssid"&&Enable==1].BSSID
```

```json
Device.WiFi.
```

#### proto

`proto`

- is optional
- type: reference
- default: `"both"`

##### proto Type

`string`

The value of this property **must** be equal to one of the [known values below](#operate-known-values).

##### proto Known Values

| Value |
| ----- |
| usp   |
| cwmp  |
| both  |

### Ubus CLI Example

```
ubus call usp operate {"path":"veniam ad consequat","action":"abrfa()","proto":"both","input":{}}
```

### JSONRPC Example

```json
{
  "jsonrpc": "2.0",
  "id": 0,
  "method": "call",
  "params": [
    "<SID>",
    "usp",
    "operate",
    { "path": "veniam ad consequat", "action": "abrfa()", "proto": "both", "input": {} }
  ]
}
```

#### output

Output will have status for sync commands and for async commands parameters as defined in TR-181-2.13

`output`

- is optional
- type: `object`

##### output Type

`object` with following properties:

| Property | Type | Required |
| -------- | ---- | -------- |
| None     | None | None     |

##### output Examples

```json
{
  "status": true
}
```

```json
{
  "AverageResponseTime": "0",
  "AverageResponseTimeDetailed": "130",
  "FailureCount": "0",
  "MaximumResponseTime": "0",
  "MaximumResponseTimeDetailed": "140",
  "MinimumResponseTime": "0",
  "MinimumResponseTimeDetailed": "120",
  "SuccessCount": "3"
}
```

### Output Example

```json
{}
```

## set

### Set handler

Set values of datamodel object element

`set`

- type: `Method`

### set Type

`object` with following properties:

| Property | Type   | Required     |
| -------- | ------ | ------------ |
| `input`  | object | **Required** |
| `output` |        | **Required** |

#### input

`input`

- is **required**
- type: `object`

##### input Type

`object` with following properties:

| Property        | Type    | Required     | Default  |
| --------------- | ------- | ------------ | -------- |
| `instance_mode` | integer | Optional     |          |
| `key`           | string  | Optional     |          |
| `path`          | string  | **Required** |          |
| `proto`         | string  | Optional     | `"both"` |
| `value`         | string  | **Required** |          |
| `values`        | object  | Optional     |          |

#### instance_mode

`instance_mode`

- is optional
- type: `integer`

##### instance_mode Type

`integer`

- minimum value: `0`
- maximum value: `1`

#### key

`key`

- is optional
- type: `string`

##### key Type

`string`

#### path

DM object path with search queries

`path`

- is **required**
- type: reference

##### path Type

`string`

- minimum length: 6 characters
- maximum length: 256 characters

##### path Examples

```json
Device.
```

```json
Device.DeviceInfo.Manufacturer
```

```json
Device.WiFi.SSID.[SSID=="test_ssid"].BSSID
```

```json
Device.WiFi.SSID.*.BSSID
```

```json
Device.WiFi.SSID.[SSID!="test_ssid"&&Enable==1].BSSID
```

```json
Device.WiFi.
```

#### proto

`proto`

- is optional
- type: reference
- default: `"both"`

##### proto Type

`string`

The value of this property **must** be equal to one of the [known values below](#set-known-values).

##### proto Known Values

| Value |
| ----- |
| usp   |
| cwmp  |
| both  |

#### value

value of the object element provided in path, path should contains valid writable object element

`value`

- is **required**
- type: `string`

##### value Type

`string`

##### value Examples

```json
{ "path": "Device.WiFi.SSID.1.SSID", "value": "test_ssid" }
```

```json
{ "path": "Device.WiFi.SSID.2.Enable", "value": "true" }
```

```json
{ "path": "Device.WiFi.SSID.1.Enable", "value": "0" }
```

#### values

To set multiple values at once, path should be relative to object elements

`values`

- is optional
- type: `object`

##### values Type

`object` with following properties:

| Property | Type | Required |
| -------- | ---- | -------- |
| None     | None | None     |

##### values Examples

```json
{ "path": "Device.WiFi.SSID.1", "values": { ".SSID": "test_ssid", ".Name": "test_name" } }
```

```json
{ "path": "Device.WiFi.SSID.2", "values": { ".SSID": "test_ssid" } }
```

### Ubus CLI Example

```
ubus call usp set {"path":"consequat","value":"est fugiat non in","proto":"usp","values":{},"key":"labore ex veniam adipisicing id","instance_mode":1}
```

### JSONRPC Example

```json
{
  "jsonrpc": "2.0",
  "id": 0,
  "method": "call",
  "params": [
    "<SID>",
    "usp",
    "set",
    {
      "path": "consequat",
      "value": "est fugiat non in",
      "proto": "usp",
      "values": {},
      "key": "labore ex veniam adipisicing id",
      "instance_mode": 1
    }
  ]
}
```

#### output

`output`

- is **required**
- type: complex

##### output Type

Unknown type ``.

```json
{
  "oneof": [
    {
      "type": "object",
      "properties": {
        "status": {
          "const": "1"
        }
      }
    },
    {
      "type": "object",
      "required": ["parameters"],
      "properties": {
        "parameters": {
          "type": "array",
          "items": {
            "type": "object",
            "properties": {
              "path": {
                "$ref": "#/definitions/path_t"
              },
              "status": {
                "const": "0"
              },
              "fault": {
                "$ref": "#/definitions/fault_t"
              }
            }
          }
        }
      }
    }
  ],
  "definitions": {
    "path_t": {
      "description": "Complete object element path as per TR181",
      "type": "string",
      "minLength": 6,
      "maxLength": 256,
      "examples": ["Device.", "Device.DeviceInfo.Manufacturer", "Device.WiFi.SSID.1.", "Device.WiFi."]
    },
    "schema_path_t": {
      "description": "Datamodel object schema path",
      "type": "string",
      "minLength": 6,
      "maxLength": 256,
      "examples": ["Device.Bridging.Bridge.{i}.", "Device.DeviceInfo.Manufacturer", "Device.WiFi.SSID.{i}.SSID"]
    },
    "boolean_t": {
      "type": "string",
      "enum": ["0", "1"]
    },
    "operate_path_t": {
      "description": "Datamodel object schema path",
      "type": "string",
      "minLength": 6,
      "maxLength": 256,
      "examples": ["Device.DHCPv4.Client.{i}.Renew()", "Device.FactoryReset()"]
    },
    "operate_type_t": {
      "type": "string",
      "enum": ["async", "sync"]
    },
    "query_path_t": {
      "description": "DM object path with search queries",
      "type": "string",
      "minLength": 6,
      "maxLength": 256,
      "examples": [
        "Device.",
        "Device.DeviceInfo.Manufacturer",
        "Device.WiFi.SSID.[SSID==\"test_ssid\"].BSSID",
        "Device.WiFi.SSID.*.BSSID",
        "Device.WiFi.SSID.[SSID!=\"test_ssid\"&&Enable==1].BSSID",
        "Device.WiFi."
      ]
    },
    "instance_t": {
      "description": "Multi object instances",
      "type": "string",
      "minLength": 6,
      "maxLength": 256
    },
    "proto_t": {
      "type": "string",
      "default": "both",
      "enum": ["usp", "cwmp", "both"]
    },
    "type_t": {
      "type": "string",
      "enum": [
        "xsd:string",
        "xsd:unsignedInt",
        "xsd:int",
        "xsd:unsignedLong",
        "xsd:long",
        "xsd:boolean",
        "xsd:dateTime",
        "xsd:hexBinary",
        "xsd:object"
      ]
    },
    "fault_t": {
      "type": "integer",
      "minimum": 7000,
      "maximum": 9050
    }
  },
  "out": "{\"oneof\":[{\"status\":\"1\"},{\"parameters\":[{\"path\":\"ex Lorem nulla\",\"status\":\"0\",\"fault\":7596},{\"path\":\"donulla\",\"status\":\"0\",\"fault\":8341},{\"path\":\"sintUt Excepteur\",\"status\":\"0\",\"fault\":8882},{\"path\":\"sint irure\",\"status\":\"0\",\"fault\":8476},{\"path\":\"veniam elit fugiat\",\"status\":\"0\",\"fault\":7593}]}],\"definitions\":{\"path_t\":{\"description\":\"Complete object element path as per TR181\",\"type\":\"string\",\"minLength\":6,\"maxLength\":256,\"examples\":[\"Device.\",\"Device.DeviceInfo.Manufacturer\",\"Device.WiFi.SSID.1.\",\"Device.WiFi.\"]},\"schema_path_t\":{\"description\":\"Datamodel object schema path\",\"type\":\"string\",\"minLength\":6,\"maxLength\":256,\"examples\":[\"Device.Bridging.Bridge.{i}.\",\"Device.DeviceInfo.Manufacturer\",\"Device.WiFi.SSID.{i}.SSID\"]},\"boolean_t\":{\"type\":\"string\",\"enum\":[\"0\",\"1\"]},\"operate_path_t\":{\"description\":\"Datamodel object schema path\",\"type\":\"string\",\"minLength\":6,\"maxLength\":256,\"examples\":[\"Device.DHCPv4.Client.{i}.Renew()\",\"Device.FactoryReset()\"]},\"operate_type_t\":{\"type\":\"string\",\"enum\":[\"async\",\"sync\"]},\"query_path_t\":{\"description\":\"DM object path with search queries\",\"type\":\"string\",\"minLength\":6,\"maxLength\":256,\"examples\":[\"Device.\",\"Device.DeviceInfo.Manufacturer\",\"Device.WiFi.SSID.[SSID==\\\"test_ssid\\\"].BSSID\",\"Device.WiFi.SSID.*.BSSID\",\"Device.WiFi.SSID.[SSID!=\\\"test_ssid\\\"&&Enable==1].BSSID\",\"Device.WiFi.\"]},\"instance_t\":{\"description\":\"Multi object instances\",\"type\":\"string\",\"minLength\":6,\"maxLength\":256},\"proto_t\":{\"type\":\"string\",\"default\":\"both\",\"enum\":[\"usp\",\"cwmp\",\"both\"]},\"type_t\":{\"type\":\"string\",\"enum\":[\"xsd:string\",\"xsd:unsignedInt\",\"xsd:int\",\"xsd:unsignedLong\",\"xsd:long\",\"xsd:boolean\",\"xsd:dateTime\",\"xsd:hexBinary\",\"xsd:object\"]},\"fault_t\":{\"type\":\"integer\",\"minimum\":7000,\"maximum\":9050}}}",
  "simpletype": "complex"
}
```

### Output Example

```json
{
  "oneof": [
    { "status": "1" },
    {
      "parameters": [
        { "path": "ex Lorem nulla", "status": "0", "fault": 7596 },
        { "path": "donulla", "status": "0", "fault": 8341 },
        { "path": "sintUt Excepteur", "status": "0", "fault": 8882 },
        { "path": "sint irure", "status": "0", "fault": 8476 },
        { "path": "veniam elit fugiat", "status": "0", "fault": 7593 }
      ]
    }
  ],
  "definitions": {
    "path_t": {
      "description": "Complete object element path as per TR181",
      "type": "string",
      "minLength": 6,
      "maxLength": 256,
      "examples": ["Device.", "Device.DeviceInfo.Manufacturer", "Device.WiFi.SSID.1.", "Device.WiFi."]
    },
    "schema_path_t": {
      "description": "Datamodel object schema path",
      "type": "string",
      "minLength": 6,
      "maxLength": 256,
      "examples": ["Device.Bridging.Bridge.{i}.", "Device.DeviceInfo.Manufacturer", "Device.WiFi.SSID.{i}.SSID"]
    },
    "boolean_t": { "type": "string", "enum": ["0", "1"] },
    "operate_path_t": {
      "description": "Datamodel object schema path",
      "type": "string",
      "minLength": 6,
      "maxLength": 256,
      "examples": ["Device.DHCPv4.Client.{i}.Renew()", "Device.FactoryReset()"]
    },
    "operate_type_t": { "type": "string", "enum": ["async", "sync"] },
    "query_path_t": {
      "description": "DM object path with search queries",
      "type": "string",
      "minLength": 6,
      "maxLength": 256,
      "examples": [
        "Device.",
        "Device.DeviceInfo.Manufacturer",
        "Device.WiFi.SSID.[SSID==\"test_ssid\"].BSSID",
        "Device.WiFi.SSID.*.BSSID",
        "Device.WiFi.SSID.[SSID!=\"test_ssid\"&&Enable==1].BSSID",
        "Device.WiFi."
      ]
    },
    "instance_t": { "description": "Multi object instances", "type": "string", "minLength": 6, "maxLength": 256 },
    "proto_t": { "type": "string", "default": "both", "enum": ["usp", "cwmp", "both"] },
    "type_t": {
      "type": "string",
      "enum": [
        "xsd:string",
        "xsd:unsignedInt",
        "xsd:int",
        "xsd:unsignedLong",
        "xsd:long",
        "xsd:boolean",
        "xsd:dateTime",
        "xsd:hexBinary",
        "xsd:object"
      ]
    },
    "fault_t": { "type": "integer", "minimum": 7000, "maximum": 9050 }
  }
}
```

## validate

### Validate a datamodel object

API to check if a datamodel object is available

`validate`

- type: `Method`

### validate Type

`object` with following properties:

| Property | Type   | Required     |
| -------- | ------ | ------------ |
| `input`  | object | **Required** |
| `output` | object | **Required** |

#### input

`input`

- is **required**
- type: `object`

##### input Type

`object` with following properties:

| Property | Type   | Required     | Default  |
| -------- | ------ | ------------ | -------- |
| `path`   | string | **Required** |          |
| `proto`  | string | Optional     | `"both"` |

#### path

DM object path with search queries

`path`

- is **required**
- type: reference

##### path Type

`string`

- minimum length: 6 characters
- maximum length: 256 characters

##### path Examples

```json
Device.
```

```json
Device.DeviceInfo.Manufacturer
```

```json
Device.WiFi.SSID.[SSID=="test_ssid"].BSSID
```

```json
Device.WiFi.SSID.*.BSSID
```

```json
Device.WiFi.SSID.[SSID!="test_ssid"&&Enable==1].BSSID
```

```json
Device.WiFi.
```

#### proto

`proto`

- is optional
- type: reference
- default: `"both"`

##### proto Type

`string`

The value of this property **must** be equal to one of the [known values below](#validate-known-values).

##### proto Known Values

| Value |
| ----- |
| usp   |
| cwmp  |
| both  |

### Ubus CLI Example

```
ubus call usp validate {"path":"irure veniam vo","proto":"usp"}
```

### JSONRPC Example

```json
{
  "jsonrpc": "2.0",
  "id": 0,
  "method": "call",
  "params": ["<SID>", "usp", "validate", { "path": "irure veniam vo", "proto": "usp" }]
}
```

#### output

`output`

- is **required**
- type: `object`

##### output Type

`object` with following properties:

| Property    | Type   | Required |
| ----------- | ------ | -------- |
| `parameter` | string | Optional |

#### parameter

`parameter`

- is optional
- type: `string`

##### parameter Type

`string`

### Output Example

```json
{ "parameter": "in" }
```
