# Function Specification

The scope of uspd is to expose the libbbfdm APIs over ubus.

```
root@iopsys:~# ubus -v list usp
'usp' @deb5f751
        "dump_schema":{}
        "list_operate":{}
        "get":{"path":"String","proto":"String","maxdepth":"Integer","next-level":"Boolean"}
        "object_names":{"path":"String","proto":"String","maxdepth":"Integer","next-level":"Boolean"}
        "instances":{"path":"String","proto":"String","maxdepth":"Integer","next-level":"Boolean"}
        "validate":{"path":"String","proto":"String","maxdepth":"Integer","next-level":"Boolean"}
        "set":{"path":"String","value":"String","values":"Table","proto":"String","key":"String"}
        "operate":{"path":"String","action":"String","input":"Table","proto":"String"}
        "add_object":{"path":"String","proto":"String","key":"String"}
        "del_object":{"path":"String","proto":"String","key":"String"}
        "getm_values":{"paths":"Array","proto":"String","next-level":"Boolean"}
        "getm_names":{"paths":"Array","proto":"String","next-level":"Boolean"}
root@iopsys:~#
root@iopsys:~# ubus -v list usp.raw
'usp.raw' @ff9d104b
        "dump_schema":{}
        "list_operate":{}
        "get":{"path":"String","proto":"String","maxdepth":"Integer","next-level":"Boolean"}
        "object_names":{"path":"String","proto":"String","maxdepth":"Integer","next-level":"Boolean"}
        "instances":{"path":"String","proto":"String","maxdepth":"Integer","next-level":"Boolean"}
        "validate":{"path":"String","proto":"String","maxdepth":"Integer","next-level":"Boolean"}
        "set":{"path":"String","value":"String","values":"Table","proto":"String","key":"String"}
        "operate":{"path":"String","action":"String","input":"Table","proto":"String"}
        "add_object":{"path":"String","proto":"String","key":"String"}
        "del_object":{"path":"String","proto":"String","key":"String"}
        "getm_values":{"paths":"Array","proto":"String","next-level":"Boolean"}
        "getm_names":{"paths":"Array","proto":"String","next-level":"Boolean"}

```

# Contents
* [usp](#usp)
* [usp.raw](#usp.raw)

## APIs

Uspd publishes two different types objects, `usp`, `usp.raw`.

### usp

An object that publishes device information.

````bash
        "dump_schema":{}
        "list_operate":{}
        "get":{"path":"String","proto":"String","maxdepth":"Integer","next-level":"Boolean"}
        "object_names":{"path":"String","proto":"String","maxdepth":"Integer","next-level":"Boolean"}
        "instances":{"path":"String","proto":"String","maxdepth":"Integer","next-level":"Boolean"}
        "validate":{"path":"String","proto":"String","maxdepth":"Integer","next-level":"Boolean"}
        "set":{"path":"String","value":"String","values":"Table","proto":"String","key":"String"}
        "operate":{"path":"String","action":"String","input":"Table","proto":"String"}
        "add_object":{"path":"String","proto":"String","key":"String"}
        "del_object":{"path":"String","proto":"String","key":"String"}
        "getm_values":{"paths":"Array","proto":"String","next-level":"Boolean"}
        "getm_names":{"paths":"Array","proto":"String","next-level":"Boolean"}
````

| Method      								|Function ID	|
| :--- 	  									| :---        	|
| [dump_schema](#dump_schema)				| 1				|
| [list_operate](#list_operate)				| 2				|
| [get](#get)					            | 3				|
| [object_names](#object_names)				| 4				|
| [instances](#instances)					| 5				|
| [validate](#validate)				 		| 6				|
| [set](#set)		 		                | 7				|
| [operate](#operate)		 		        | 8				|
| [add_object](#add_object)			        | 9   			|
| [del_object](#del_object)		            | 10			|
| [getm_values](#getm_values)	            | 11			|
| [getm_names](#getm_names)			        | 12			|

#### Methods

Method descriptions of the `usp` object.

##### dump_schema

Exposes data model schema registered.

* [dump_schema documentation](./api/usp.md#dump_schema)

##### list_operate

Exposes various synch and asynch operations e.g., IPPing(), NeighbourDiagnostics() etc.

* [list_operate documentation](./api/usp.md#list_operate)

##### get

Exposes all information regarding various registered schema parameters.

* [get documentation](./api/usp.md#get)

##### object_names

Exposes various object names.

* [object_names documentation](./api/usp.md#object_names)

##### instances

Get all the instances for specified schema path.

* [disconnect documentation](./api/usp.md#instances)

##### validate

Validate whether the path provided is valid as per registerd schema paths.

* [validate documentation](./api/usp.md#validate)

##### set

Set information regarding various registered schema parameters.

* [set documentation](./api/usp.md#set)

##### operate

Execute various synch/asynch operations e.g., IPPing(), NeighbourDiagnostics() etc.

* [operate documentation](./api/usp.md#operate)

##### add_object

Add object parameter to a specific registered schema.

* [add_object neighbor documentation](./api/usp.md#add_object)

##### del_object

Del object parameter to a specific registered schema.

* [del_object documentation](./api/usp.md#del_object)

##### getm_values

An extension to get method, this method can be use to get parameter values for multiple query paths at once.

* [getm_values documentation](./api/usp.md#getm_values)

##### getm_names

An extension to get method, this method can be use to get names parameter for multiple query paths at once.

* [getm_names documentation](./api/usp.md#getm_names)

### usp.raw

Object for device functionality. One object per device will be published to
ubus.

````bash
        "dump_schema":{}
        "list_operate":{}
        "get":{"path":"String","proto":"String","maxdepth":"Integer","next-level":"Boolean"}
        "object_names":{"path":"String","proto":"String","maxdepth":"Integer","next-level":"Boolean"}
        "instances":{"path":"String","proto":"String","maxdepth":"Integer","next-level":"Boolean"}
        "validate":{"path":"String","proto":"String","maxdepth":"Integer","next-level":"Boolean"}
        "set":{"path":"String","value":"String","values":"Table","proto":"String","key":"String"}
        "operate":{"path":"String","action":"String","input":"Table","proto":"String"}
        "add_object":{"path":"String","proto":"String","key":"String"}
        "del_object":{"path":"String","proto":"String","key":"String"}
        "getm_values":{"paths":"Array","proto":"String","next-level":"Boolean"}
        "getm_names":{"paths":"Array","proto":"String","next-level":"Boolean"}

````

| Method      								|Function ID	|
| :--- 	  									| :---        	|
| [dump_schema](#dump_schema_raw)			| 1				|
| [list_operate](#list_operate_raw)			| 2				|
| [get](#get_raw)					        | 3				|
| [object_names](#object_names_raw)			| 4				|
| [instances](#instances_raw)				| 5				|
| [validate](#validate_raw)				 	| 6				|
| [set](#set_raw)		 		            | 7				|
| [operate](#operate_raw)		 		    | 8				|
| [add_object](#add_object_raw)			    | 9   			|
| [del_object](#del_object_raw)		        | 10			|
| [getm_values](#getm_values_raw)	        | 11			|
| [getm_names](#getm_names_raw)			    | 12			|

#### Methods

Method descriptions of the `usp.raw` object.

##### dump_schema_raw

Exposes data model schema registered.

* [dump_schema documentation](./api/usp.raw.md#dump_schema)

##### list_operate_raw

Exposes various synch and asynch operations e.g., IPPing(), NeighbourDiagnostics() etc.

* [list_operate documentation](./api/usp.raw.md#list_operate)

##### get_raw

Exposes all information regarding various registered schema parameters.

* [get documentation](./api/usp.raw.md#get)

##### object_names_raw

Exposes various object names.

* [object_names documentation](./api/usp.raw.md#object_names)

##### instances_raw

Get all the instances for specified schema path.

* [disconnect documentation](./api/usp.raw.md#instances)

##### validate_raw

Validate whether the path provided is valid as per registerd schema paths.

* [validate documentation](./api/usp.raw.md#validate)

##### set_raw

Set information regarding various registered schema parameters.

* [set documentation](./api/usp.raw.md#set)

##### operate_raw

Execute various synch/asynch operations e.g., IPPing(), NeighbourDiagnostics() etc.

* [operate documentation](./api/usp.raw.md#operate)

##### add_object_raw

Add object parameter to a specific registered schema.

* [add_object neighbor documentation](./api/usp.raw.md#add_object)

##### del_object_raw

Del object parameter to a specific registered schema.

* [del_object documentation](./api/usp.raw.md#del_object)

##### getm_values_raw

An extension to get method, this method can be use to get parameter values for multiple query paths at once.

* [getm_values documentation](./api/usp.raw.md#getm_values)

##### getm_names_raw

An extension to get method, this method can be use to get names parameter for multiple query paths at once.

* [getm_names documentation](./api/usp.raw.md#getm_names)

