#ifndef EVENT_H
#define EVENT_H

#include "usp.h"
#include "common.h"

void list_event_schema(struct blob_buf *bb);
bool is_registered_event(char *name);
#endif /* EVENT_H */
