/*
 * pretty_print.c: utils for pretty printing of results
 *
 * Copyright (C) 2020 iopsys Software Solutions AB. All rights reserved.
 *
 * Author: Vivek Dutta <vivek.dutta@iopsys.eu>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA
 */

#include "common.h"
#include "get_helper.h"
#include "pretty_print.h"
#include <libbbf_api/dmbbf.h>
#include <libbbfdm/dmbbfcommon.h>


// private function and structures
struct resultstack {
	void *cookie;
	char *key;
	struct list_head list;
};

static bool is_search_by_reference(char *path)
{
	size_t pindex = 0, bindex = 0;
	char *last_plus, *last_bracket;

	DEBUG("Entry |%s|", path);

	if (match(path, "[+]+")) {
		last_bracket = strrchr(path, ']');
		if (!last_bracket)
			return true;

		last_plus = strrchr(path, '+');

		pindex = (size_t)labs(last_plus - path);
		bindex = (size_t)labs(last_bracket - path);

		if (pindex > bindex)
			return true;
	}

	return false;
}

//if matched start will have first match index, end will have end index
static bool is_res_required(char *str, size_t *start, size_t *len)
{
	size_t i = 0, index = 0;
	char temp_char[MAX_DM_KEY_LEN] = {'\0'};
	size_t s_len, b_len, p_len;
	char *star, *b_start, *b_end, *plus;

	DEBUG("Entry |%s|", str);
	if (match(str, GLOB_CHAR)) {
		s_len = strlen(str);
		b_len = s_len;
		p_len = s_len;

		star = strchr(str, '*');
		b_start = strchr(str, '[');
		b_end = strchr(str, ']');
		plus = strchr(str, '+');

		if (star)
			s_len = (size_t)labs(star - str);

		if (b_start)
			b_len = (size_t)labs(b_start - str);

		if (plus)
			p_len = (size_t)labs(plus - str);

		*start = MIN(MIN(s_len, p_len), b_len);
		if (*start == s_len) {
			*len = 1;
		} else if (*start == p_len) {
			i = 0, index = 0;

			while ((str+i) != plus) {
				if (str[i] == DELIM)
					index = i;
				++i;
			}
			*start = index+1;
			*len = p_len - index;
		} else {
			*len = (size_t)labs(b_end - b_start);
		}

		// Check if naming with aliases used
		strncpy(temp_char, str + *start, *len);
		if (match(temp_char, GLOB_EXPR))
			return true;

		if (match(temp_char, "[*+]+"))
			return true;
	}
	*start = strlen(str);
	return false;
}

static size_t get_glob_len(char *path)
{
	size_t m_index = 0, m_len = 0, ret = 0;
	size_t plen = strlen(path);
	char temp_name[MAX_DM_KEY_LEN] = {'\0'};
	char *end = NULL;
	char name[MAX_DM_KEY_LEN] = {'\0'};

	DEBUG("Entry");
	if (is_res_required(path, &m_index, &m_len)) {
		strncpy(temp_name, path, m_index - 1);
		end = strrchr(temp_name, DELIM);
		ret = m_index - strlen(end);
	} else {
		if (path[plen - 1] == DELIM) {
			strncpy(name, path, plen - 1);
		} else {
			ret = 1;
			strncpy(name, path, plen);
		}
		end = strrchr(name, DELIM);
		if (end == NULL)
			return ret;

		ret = ret + strlen(path) - strlen(end);
		if (is_node_instance(end+1)) {
			strncpy(temp_name, path, plen - strlen(end) - 1);
			end = strrchr(temp_name, DELIM);
			ret = ret - strlen(end);
		}
	}
	return(ret);
}

static void resulting(uint8_t maxdepth, char *path, char *qPath, struct dmctx *bbf_ctx, struct list_head *pv_local)
{
	struct dm_parameter *n;
	uint8_t count;

	size_t plen = get_glob_len(qPath);
	//size_t plen = 0;
	size_t path_len = strlen(path);

	list_for_each_entry(n, &bbf_ctx->list_parameter, list) {
		if (path[path_len - 1] == DELIM) {
			if (!strncmp(n->name, path, path_len)) {
				if (is_search_by_reference(qPath))
					plen = 0;

				if (maxdepth > 4 || maxdepth == 0) {
					add_pv_node(n->name + plen, n->data, n->type, pv_local);
				} else {
					count = count_delim(n->name + path_len);
					if (count < maxdepth)
						add_pv_node(n->name + plen, n->data, n->type, pv_local);
				}
			}
		} else {
			if (!strcmp(n->name, path)) {
				if (is_search_by_reference(qPath))
					plen = 0;

				if (maxdepth > 4 || maxdepth == 0) {
					add_pv_node(n->name + plen, n->data, n->type, pv_local);
				} else {
					count = count_delim(n->name + path_len);
					if (count < maxdepth)
						add_pv_node(n->name + plen, n->data, n->type, pv_local);
				}
			}
		}
	}
}

static void add_data_blob(struct blob_buf *bb, char *param, char *value, char *type)
{
	if (param == NULL || value == NULL || type == NULL)
		return;

	DEBUG("# Adding BLOB (%s)::(%s)", param, value);
	switch (get_dm_type(type)) {
	case DMT_UNINT:
		blobmsg_add_u64(bb, param, (uint32_t)atoi(value));
		break;
	case DMT_INT:
		blobmsg_add_u32(bb, param, atoi(value));
		break;
	case DMT_LONG:
		blobmsg_add_u64(bb, param, atoll(value));
		break;
	case DMT_UNLONG:
		blobmsg_add_u64(bb, param, (uint64_t)atoll(value));
		break;
	case DMT_BOOL:
		if (get_boolean_string(value))
			blobmsg_add_u8(bb, param, true);
		else
			blobmsg_add_u8(bb, param, false);
		break;
	default: //"xsd:hexbin" "xsd:dateTime" "xsd:string"
		bb_add_string(bb, param, value);
		break;
	}
}

static void free_result_list(struct list_head *head)
{
	struct resultstack *iter, *node;

	list_for_each_entry_safe(iter, node, head, list) {
		free(iter->key);
		list_del(&iter->list);
		free(iter);
	}
}

static void free_result_node(struct resultstack *rnode)
{
	if (rnode) {
		DEBUG("## ResStack DEL(%s)", rnode->key);
		free(rnode->key);
		list_del(&rnode->list);
		free(rnode);
	}
}

static void add_result_node(struct list_head *rlist, char *key, char *cookie)
{
	struct resultstack *rnode = NULL;

	rnode = (struct resultstack *) malloc(sizeof(*rnode));
	if (!rnode) {
		ERR("Out of memory!");
		return;
	}

	rnode->key = (key) ? strdup(key) : strdup("");
	rnode->cookie = cookie;
	DEBUG("## ResSTACK ADD (%s) ##", rnode->key);

	INIT_LIST_HEAD(&rnode->list);
	list_add(&rnode->list, rlist);
}

static bool is_leaf_element(char *path)
{
	char *ptr = NULL;

	if (!path)
		return true;

	ptr = strchr(path, DELIM);

	return (ptr == NULL);
}

static bool get_next_element(char *path, char *param)
{
	char *ptr;
	size_t len;

	if (!path)
		return false;

	len = strlen(path);
	ptr = strchr(path, DELIM);
	if (ptr)
		strncpyt(param, path, (size_t)labs(ptr - path) + 1);
	else
		strncpyt(param, path, len + 1);

	return true;
}

static bool is_same_group(char *path, char *group)
{
	return (strncmp(path, group, strlen(group)) == 0);
}

static bool add_paths_to_stack(struct blob_buf *bb, char *path, size_t begin,
			       struct pvNode *pv, struct list_head *result_stack)
{
	char key[MAX_DM_KEY_LEN], param[MAX_DM_PATH], *ptr;
	size_t parsed_len = 0;
	void *c;
	char *k;


	ptr = path + begin;
	if (is_leaf_element(ptr)) {
		add_data_blob(bb, ptr, pv->val, pv->type);
		return true;
	}

	while (get_next_element(ptr, key)) {
		parsed_len += strlen(key) + 1;
		ptr += strlen(key) + 1;
		if (is_leaf_element(ptr)) {
			strncpyt(param, path, begin + parsed_len + 1);
			if (is_node_instance(key))
				c = blobmsg_open_table(bb, NULL);
			else
				c = blobmsg_open_table(bb, key);

			k = param;
			add_result_node(result_stack, k, c);
			add_data_blob(bb, ptr, pv->val, pv->type);
			break;
		}
		strncpyt(param, pv->param, begin + parsed_len + 1);
		if (is_node_instance(ptr))
			c = blobmsg_open_array(bb, key);
		else
			c = blobmsg_open_table(bb, key);

		k = param;
		add_result_node(result_stack, k, c);
	}

	return true;
}

// public functions
void prepare_result_blob(struct blob_buf *bb, struct list_head *pv_list)
{
	char *ptr;
	size_t len = 0;
	struct pvNode *pv;
	struct resultstack *rnode;

	LIST_HEAD(result_stack);

	if (!bb || !pv_list)
		return;

	if (list_empty(pv_list))
		return;

	list_for_each_entry(pv, pv_list, list) {
		ptr = pv->param;
		if (list_empty(&result_stack)) {
			DEBUG("stack empty Processing (%s)", ptr);
			add_paths_to_stack(bb, pv->param, 0, pv, &result_stack);
		} else {
			bool is_done = false;

			while (is_done == false) {
				rnode = list_entry(result_stack.next, struct resultstack, list);
				if (is_same_group(ptr, rnode->key)) {
					len = strlen(rnode->key);
					ptr = ptr + len;

					DEBUG("GROUP (%s), ptr(%s), len(%d)", pv->param, ptr, len);
					add_paths_to_stack(bb, pv->param, len, pv, &result_stack);
					is_done = true;
				} else {
					// Get the latest entry before deleting it
					DEBUG("DIFF GROUP pv(%s), param(%s)", pv->param, ptr);
					blobmsg_close_table(bb, rnode->cookie);
					free_result_node(rnode);
					if (list_empty(&result_stack)) {
						add_paths_to_stack(bb, pv->param, 0, pv, &result_stack);
						is_done = true;
					}
				}
			}
		}
	}

	// Close the stack entry if left
	list_for_each_entry(rnode, &result_stack, list) {
		blobmsg_close_table(bb, rnode->cookie);
	}
	free_result_list(&result_stack);
}

void prepare_pretty_result(uint8_t maxdepth, char *qPath, struct blob_buf *bb,
			   struct dmctx *bbf_ctx, struct list_head *rslvd)
{
	struct pathNode *iter = NULL;

	LIST_HEAD(pv_local);

	list_for_each_entry(iter, rslvd, list) {
		resulting(maxdepth, iter->path, qPath, bbf_ctx, &pv_local);
	}

	struct pvNode *pv;

	DEBUG("################### DATA to PROCESS ##################");
	list_for_each_entry(pv, &pv_local, list) {
		DEBUG("## %s ##", pv->param);
	}
	DEBUG("######################################################");

	prepare_result_blob(bb, &pv_local);

	free_pv_list(&pv_local);
}

void dump_pv_list(struct list_head *pv_list)
{
	struct pvNode *pv;

	INFO("############### PV list Dump #########");
	list_for_each_entry(pv, pv_list, list) {
		INFO("## (%s)::(%s)::(%s) ##", pv->param, pv->val, pv->type);
	}
	INFO("############# dump done ###############");
}

void dump_resolved_list(struct list_head *resolved_list)
{
	struct pathNode *iter;

	INFO("********************Resolved List Dump***********************");
	list_for_each_entry(iter, resolved_list, list) {
		INFO("## %s ##", iter->path);
	}
	INFO("**************************DONE*******************************");
}

void prepare_result_raw(struct blob_buf *bb, struct dmctx *bbf_ctx, struct list_head *rslvd)
{
	struct pathNode *iter = NULL;
	struct dm_parameter *n;
	void *array, *table;
	size_t ilen;

	array = blobmsg_open_array(bb, "parameters");
	list_for_each_entry(iter, rslvd, list) {
		ilen = strlen(iter->path);
		list_for_each_entry(n, &bbf_ctx->list_parameter, list) {
			if (iter->path[ilen - 1] == DELIM) {
				if (!strncmp(n->name, iter->path, ilen)) {
					table = blobmsg_open_table(bb, NULL);
					bb_add_string(bb, "parameter", n->name);
					bb_add_string(bb, "value", n->data);
					bb_add_string(bb, "type", n->type);
					blobmsg_close_table(bb, table);
				}
			} else {
				if (!strcmp(n->name, iter->path)) {
					table = blobmsg_open_table(bb, NULL);
					bb_add_string(bb, "parameter", n->name);
					bb_add_string(bb, "value", n->data);
					bb_add_string(bb, "type", n->type);
					blobmsg_close_table(bb, table);
				}
			}
		}
	}
	blobmsg_close_array(bb, array);
}
