/*
 * events.c: Handler to generate usp events on ubus
 *
 * Copyright (C) 2021 iopsys Software Solutions AB. All rights reserved.
 *
 * Author: Vivek Dutta <vivek.dutta@iopsys.eu>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA
 */

#include "common.h"
#include "events.h"
#include <libubus.h>

struct event {
	const char *path;
	const char **params;
};

static const struct event supp_events[] = {
	{
		.path = "Device.LocalAgent.TransferComplete!", 
		.params = (const char *[]) {
			"Command",
			"CommandKey",
			"Requestor",
			"TransferType",
			"Affected",
			"TransferURL",
			"StartTime",
			"CompleteTime",
			"FaultCode",
			"FaultString",
			NULL
		}
	}
};

void list_event_schema(struct blob_buf *bb)
{
	void *array, *table, *array_arg;
	size_t i, j;
	const struct event *ev;

	array = blobmsg_open_array(bb, "parameters");
	for (i = 0; i < ARRAY_SIZE(supp_events); i++) {
		ev = &supp_events[i];
		table = blobmsg_open_table(bb, NULL);
		blobmsg_add_string(bb, "parameter", ev->path);
		// filling params
		if (ev->params) {
			array_arg = blobmsg_open_array(bb, "in");
			for (j = 0; ev->params[j] != NULL; j++)
				blobmsg_add_string(bb, NULL, ev->params[j]);

			blobmsg_close_array(bb, array_arg);
		}
		blobmsg_close_table(bb, table);
	}

	blobmsg_close_array(bb, array);
}

bool is_registered_event(char *name)
{
	const struct event *ev;
	size_t i;

	for (i = 0; i < ARRAY_SIZE(supp_events); i++) {
		ev = &supp_events[i];
		if (strcmp(name, ev->path) == 0)
			return true;
	}

	return false;
}
