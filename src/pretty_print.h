/*
 * pretty_print.h: utils for pretty printing of results
 *
 * Copyright (C) 2020 iopsys Software Solutions AB. All rights reserved.
 *
 * Author: Vivek Dutta <vivek.dutta@iopsys.eu>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA
 */

#ifndef PRETTY_PRINT_H
#define PRETTY_PRINT_H

void prepare_result_blob(struct blob_buf *bb, struct list_head *pv_list);
void prepare_pretty_result(uint8_t maxdepth, char *qPath, struct blob_buf *bb,
			   struct dmctx *bbf_ctx, struct list_head *rslvd);
void prepare_result_raw(struct blob_buf *bb, struct dmctx *bbf_ctx, struct list_head *rslvd);
void dump_pv_list(struct list_head *pv_list);
void dump_resolved_list(struct list_head *resolved_list);

#endif
