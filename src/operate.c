/*
 * operate.c: Operate handler for uspd
 *
 * Copyright (C) 2019 iopsys Software Solutions AB. All rights reserved.
 *
 * Author: Vivek Dutta <vivek.dutta@iopsys.eu>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA
 */

#include "common.h"
#include "operate.h"
#include "get_helper.h"
#include "pretty_print.h"
#include "ipc.h"
#include <libbbfdm/dmbbfcommon.h>
#include <libubus.h>

static void usp_operate_cmd(usp_data_t *data, struct blob_buf *bb)
{
	struct pathNode *rv;
	void *array, *table;
	char path[MAX_DM_PATH];
	struct dmctx bbf_ctx;
	int fault;
	size_t len;

	memset(&bbf_ctx, 0, sizeof(struct dmctx));

	set_bbfdatamodel_type(data->proto);
	bbf_init(&bbf_ctx, data->instance);

	LIST_HEAD(resolved_paths);

	fault = get_resolved_paths(&bbf_ctx, data->qpath, &resolved_paths);
	bbf_cleanup(&bbf_ctx);
	if (fault) {
		fill_err_code(bb, fault);
	} else {
		array = blobmsg_open_array(bb, "Results");
		list_for_each_entry(rv, &resolved_paths, list) {
			len = strlen(rv->path);
			strncpyt(path, rv->path, len + 1);
			strcat(path, data->op_action);

			table = blobmsg_open_table(bb, NULL);
			blobmsg_add_string(bb, "path", path);
			usp_dm_operate(bb, path, data->op_input, data->is_raw, data->instance);
			blobmsg_close_table(bb, table);
		}
		blobmsg_close_array(bb, array);
	}

	free_path_list(&resolved_paths);
}

void list_operate_schema(struct blob_buf *bb)
{
	struct dm_parameter *n;
	void *array, *table, *array_arg;
	struct dmctx bbf_ctx;
	operation_args *args;
	size_t i;
	const char **ap;

	memset(&bbf_ctx, 0, sizeof(struct dmctx));

	set_bbfdatamodel_type(BBFDM_USP);
	bbf_init(&bbf_ctx, INSTANCE_MODE_NUMBER);

	bbf_dm_list_operate(&bbf_ctx);

	array = blobmsg_open_array(bb, "parameters");
	list_for_each_entry(n, &bbf_ctx.list_parameter, list) {

		table = blobmsg_open_table(bb, NULL);
		blobmsg_add_string(bb, "parameter", n->name);
		blobmsg_add_string(bb, "type", n->type);

		DEBUG("Operate node|%s|, type(%s)", n->name, n->type);

		// filling in and out parameter
		if (n->data) {
			args = (operation_args *) n->data;

			ap = args->in;
			if (ap) {
				array_arg = blobmsg_open_array(bb, "in");
				for (i = 0; ap[i] != NULL; i++)
					blobmsg_add_string(bb, NULL, ap[i]);

				blobmsg_close_array(bb, array_arg);
			}

			ap = args->out;
			if (ap) {
				array_arg = blobmsg_open_array(bb, "out");
				for (i = 0; ap[i] != NULL; i++)
					blobmsg_add_string(bb, NULL, ap[i]);

				blobmsg_close_array(bb, array_arg);
			}
		}
		blobmsg_close_table(bb, table);
	}

	blobmsg_close_array(bb, array);
	bbf_cleanup(&bbf_ctx);
}

void usp_operate_cmd_async(usp_data_t *data, void *output)
{
	struct blob_buf bb;
	size_t blob_len = 0;

	memset(&bb, 0, sizeof(struct blob_buf));
	blob_buf_init(&bb, 0);

	usp_operate_cmd(data, &bb);

	blob_len = blob_pad_len(bb.head);
	if (blob_len >= DEF_IPC_DATA_LEN) {
		ERR("Failed to set Blob data len(%zd), def(%zd)", blob_len, DEF_IPC_DATA_LEN);
		blob_buf_free(&bb);
		blob_buf_init(&bb, 0);
		fill_err_code(&bb, FAULT_9002);
		blob_len = blob_pad_len(bb.head);
	}

	memcpy(output, bb.head, blob_len);

	// free
	blob_buf_free(&bb);
}

void usp_operate_cmd_sync(usp_data_t *data)
{
	struct blob_buf bb;

	memset(&bb, 0, sizeof(struct blob_buf));
	blob_buf_init(&bb, 0);

	usp_operate_cmd(data, &bb);

	ubus_send_reply(data->ctx, data->req, bb.head);

	// free
	blob_buf_free(&bb);
}
